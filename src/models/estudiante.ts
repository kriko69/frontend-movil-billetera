export class estudiante{
    usuario_id: number;
    nombre: string;
    apellidos: string;
    carnet: number;
    correo: string;
    telefono: number;
    fecha_nacimiento:Date;
    carrera:string;
    fecha_con_formato:string;
    imagen:string;
    constructor()
    {
        this.usuario_id=0;
        this.nombre= "";
        this.apellidos= "";
        this.correo= "";
        this.carrera="";
    }
}