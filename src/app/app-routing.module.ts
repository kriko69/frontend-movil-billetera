import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'login',
    loadChildren: () => import('./estudiante/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registro-proveedor',
    loadChildren: () => import('./proveedor/registro-proveedor/registro-proveedor.module').then( m => m.RegistroProveedorPageModule)
  },
  {
    path: 'registro-negocio',
    loadChildren: () => import('./proveedor/registro-negocio/registro-negocio.module').then( m => m.RegistroNegocioPageModule)
  },
  {
    path: 'menu-proveedor',
    loadChildren: () => import('./proveedor/menu-proveedor/menu-proveedor.module').then( m => m.MenuProveedorPageModule)
  },
  {
    path: 'menu-estudiante',
    loadChildren: () => import('./estudiante/menu-estudiante/menu-estudiante.module').then( m => m.MenuEstudiantePageModule)
  },
  {
    path: 'servicios',
    loadChildren: () => import('./estudiante/servicios/servicios.module').then( m => m.ServiciosPageModule)
  },
  {
    path: 'reset-password',
    loadChildren: () => import('./reset-password/reset-password.module').then( m => m.ResetPasswordPageModule)
  },
  {
    path: 'cambiar-password',
    loadChildren: () => import('./cambiar-password/cambiar-password.module').then( m => m.CambiarPasswordPageModule)
  },
  {
    path: 'perfil',
    loadChildren: () => import('./estudiante/perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'tipo-movimientos',
    loadChildren: () => import('./estudiante/tipo-movimientos/tipo-movimientos.module').then( m => m.TipoMovimientosPageModule)
  },
  {
    path: 'movimientos',
    loadChildren: () => import('./estudiante/movimientos/movimientos.module').then( m => m.MovimientosPageModule)
  },
  {
    path: 'transferencia',
    loadChildren: () => import('./estudiante/transferencia/transferencia.module').then( m => m.TransferenciaPageModule)
  },
  {
    path: 'negocios-pago',
    loadChildren: () => import('./estudiante/negocios-pago/negocios-pago.module').then( m => m.NegociosPagoPageModule)
  },
  {
    path: 'pago',
    loadChildren: () => import('./estudiante/pago/pago.module').then( m => m.PagoPageModule)
  },
  {
    path: 'cuentas',
    loadChildren: () => import('./proveedor/cuentas/cuentas.module').then( m => m.CuentasPageModule)
  },
  {
    path: 'negocios-qr',
    loadChildren: () => import('./proveedor/negocios-qr/negocios-qr.module').then( m => m.NegociosQrPageModule)
  },
  {
    path: 'codigo-qr',
    loadChildren: () => import('./proveedor/codigo-qr/codigo-qr.module').then( m => m.CodigoQrPageModule)
  },
  {
    path: 'mis-negocios',
    loadChildren: () => import('./proveedor/mis-negocios/mis-negocios.module').then( m => m.MisNegociosPageModule)
  },
  {
    path: 'perfil-negocios',
    loadChildren: () => import('./proveedor/perfil-negocios/perfil-negocios.module').then( m => m.PerfilNegociosPageModule)
  },
  {
    path: 'movimientos-proveedor',
    loadChildren: () => import('./proveedor/movimientos-proveedor/movimientos-proveedor.module').then( m => m.MovimientosProveedorPageModule)
  },
  {
    path: 'reintentar-afiliacion',
    loadChildren: () => import('./proveedor/reintentar-afiliacion/reintentar-afiliacion.module').then( m => m.ReintentarAfiliacionPageModule)
  },
  {
    path: 'foto-perfil',
    loadChildren: () => import('./estudiante/foto-perfil/foto-perfil.module').then( m => m.FotoPerfilPageModule)
  },
  {
    path: 'tema',
    loadChildren: () => import('./estudiante/tema/tema.module').then( m => m.TemaPageModule)
  },
  {
    path: 'gestiones',
    loadChildren: () => import('./estudiante/gestiones/gestiones.module').then( m => m.GestionesPageModule)
  },
  {
    path: 'mensualidades',
    loadChildren: () => import('./estudiante/mensualidades/mensualidades.module').then( m => m.MensualidadesPageModule)
  },
  {
    path: 'pago-mensualidad',
    loadChildren: () => import('./estudiante/pago-mensualidad/pago-mensualidad.module').then( m => m.PagoMensualidadPageModule)
  },
  {
    path: 'tipo-mensualidad',
    loadChildren: () => import('./estudiante/tipo-mensualidad/tipo-mensualidad.module').then( m => m.TipoMensualidadPageModule)
  },
  {
    path: 'mensualidades-pagadas',
    loadChildren: () => import('./estudiante/mensualidades-pagadas/mensualidades-pagadas.module').then( m => m.MensualidadesPagadasPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
