import { TestBed } from '@angular/core/testing';

import { ParametersServiceService } from './parameters-service.service';

describe('ParametersServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParametersServiceService = TestBed.get(ParametersServiceService);
    expect(service).toBeTruthy();
  });
});
