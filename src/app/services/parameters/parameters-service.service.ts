import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { proveedor } from 'src/models/proveedor';
import { estudiante } from 'src/models/estudiante';

@Injectable({
  providedIn: 'root'
})
export class ParametersServiceService {

  
  public rol:string;
  public token:string;
  public proveedor:proveedor;
  public ServidorUrl='http:///157.245.140.17/api/';
  //public ServidorUrl='http:///localhost:8000/api/';
  public ImagenesempresaUrl='http://167.172.238.126/imagenes/empresas/';
  public ImagenesusuarioUrl='http://167.172.238.126/imagenes/usuarios/';
  public ImagenesactividadUrl='http://167.172.238.126/imagenes/actividades/';
  public defaultimageuser='http://167.172.238.126/imagenes/usuarios/defaultuser.png';
  public defaultimageact='http://167.172.238.126/imagenes/actividades/defaultact.png';
  
  public imagenUsuarioEstudiante='http://157.245.140.17/imagenes/usuarios/estudiantes/';
  public imagenUsuarioProveedor='http://157.245.140.17/imagenes/usuarios/proveedores/';
  public estudianteRegistro:estudiante;
  public proveedorLogin:proveedor;
  public estudiantePefil:estudiante;
  public carnetResetPassword:number;
  public tipoMovimiento:string;
  public soyProveedor:boolean=false;
  public irLogin:boolean=true;
  public cuentaUsuario:number;
  public negocio_id:number;
  public qrData:string;
  public numeroCuentaMovimientosProveedor:number;
  public negocioIdParaQr:number;
  public numeroCuentaParaQr:number;
  public todoElNegocio:Object;
  public nombreNegocioPago:string;
  public nombreFotoPerfil:string;
  public gestion;
  public mensualidad;
  public mensualidadPagada;
  constructor(public navController:NavController) {
    this.rol="";
    this.token=""; 
    
    }

    public getMensaulidad()
    {
      return this.mensualidad;
    }
    public setMensaulidad(m:any)
    {
      this.mensualidad=m;
    }
    public getMmensualidadPagada()
    {
      return this.mensualidadPagada;
    }
    public setMmensualidadPagada(m:any)
    {
      this.mensualidadPagada=m;
    }
    public getGestion()
    {
      return this.gestion;
    }
    public setGestion(g:any)
    {
      this.gestion=g;
    }
    public getimagenUsuarioEstudiante()
    {
      return this.imagenUsuarioEstudiante;
    }
    public getimagenUsuarioProveedor()
    {
      return this.imagenUsuarioProveedor;
    }
    public getNombreFotoPerfil()
    {
      return this.nombreFotoPerfil;
    }
    public setNombreFotoPerfil(n:string)
    {
      this.nombreFotoPerfil=n;
    }
    public getNombreNegocioPago()
    {
      return this.nombreNegocioPago;
    }
    public setNombreNegocioPago(n:string)
    {
      this.nombreNegocioPago=n;
    }
    public getTodoElNegocio(){
      return this.todoElNegocio;
    }
    public setTodoElNegocio(negocio:object){
      this.todoElNegocio=negocio;
    }
    public getNumeroCuentaIdParaQr(){
      return this.numeroCuentaParaQr;
    }
    public setNumeroCuentaIdParaQr(nc:number){
      this.numeroCuentaParaQr=nc;
    }
    public getNegocioIdParaQr(){
      return this.negocioIdParaQr;
    }
    public setNegocioIdParaQr(nid:number){
      this.negocioIdParaQr=nid;
    }
    public getNumeroCuentaMovimientosProveedor(){
      return this.numeroCuentaMovimientosProveedor;
    }
    public setNumeroCuentaMovimientosProveedor(nc:number){
      this.numeroCuentaMovimientosProveedor=nc;
    }
    public getQrData(){
      return this.qrData;
    }
    public setQrData(data:string){
      this.qrData=data;
    }
    public getNegocioId(){
      return this.negocio_id;
    }
    public setNegocioId(id:number){
      this.negocio_id=id;
    }

    public getCuentaUsuario(){
      return this.cuentaUsuario;
    }
    public setCuentaUsuario(c:number){
      this.cuentaUsuario=c;
    }
    public getIrLogin(){
      return this.irLogin;
    }
    public setIrLogin(il:boolean){
      this.irLogin=il;
    }
    public getSoyProveedor(){
      return this.soyProveedor;
    }
    public setSoyProveedor(sp:boolean){
      this.soyProveedor=sp;
    }
    public getProveedorLogin(){
      return this.proveedorLogin;
    }
    public setProveedorLogin(p:proveedor){
      this.proveedorLogin=p;
    }
    public getTipoMovimiento(){
      return this.tipoMovimiento;
    }
    public setTipoMovimiento(tm:string){
      this.tipoMovimiento=tm;
    }
    public getCarnetResetPassword(){
      return this.carnetResetPassword;
    }
    public setCarnetResetPassword(c:number){
      this.carnetResetPassword=c;
    }
    public getEstudianteRegistro(){
      return this.estudianteRegistro;
    }
    public setEstudianteRegistro(e:estudiante){
      this.estudianteRegistro=e;
    }
    public getProveedorRegistro(){
      return this.proveedor;
    }
    public setProveedorRegistro(p:proveedor){
      this.proveedor=p;
    }
    public getPerfilLogin(){
      return this.estudiantePefil;
    }
    public setPerfilLogin(e:estudiante){
      this.estudiantePefil=e;
    }
    public getToken(){
      return this.token;
    }
    public setToken(tk:string){
      this.token=tk;
    }
    public getRol(){
      return this.rol;
    }
    public setRol(rol:string){
      this.rol=rol;
    }
    
    
    
}
