import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';
import { ParametersServiceService } from './parameters/parameters-service.service';
import { proveedor } from 'src/models/proveedor';

@Injectable({
  providedIn: 'root'
})
export class ProveedorServiceService {

  constructor(public http:HttpClient,
    public parameter:ParametersServiceService,
    public http2:Http) {

  }

  registroProveedor(proveedor:proveedor)
  {
    let data={
      "nombre":proveedor.nombre,
      "apellidos":proveedor.apellidos,
      "carnet":proveedor.carnet,
      "correo":proveedor.correo,
      "fecha_nacimiento":proveedor.fecha_nacimiento,
      "telefono":proveedor.telefono,
      "password":proveedor.password,
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);

    return this.http.post(this.parameter.ServidorUrl+'registrar-vendedor', opsi, header);
  }

  perfil(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'perfil',{ headers:header}).pipe(map((res=>res.json())));
  }
}
