import { TestBed } from '@angular/core/testing';

import { CifradoHttpService } from './cifrado-http.service';

describe('CifradoHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CifradoHttpService = TestBed.get(CifradoHttpService);
    expect(service).toBeTruthy();
  });
});
