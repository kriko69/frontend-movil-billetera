import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Storage } from '@ionic/storage';
import { PresentersServiceService } from './presenters/presenters-service.service';

@Injectable({
  providedIn: 'root'
})
export class PushService {

  userId:String;
  constructor(private oneSignal:OneSignal,
    private storage:Storage,
    private presenters:PresentersServiceService) { 

  }

  configuracionInicial()
  {
    this.oneSignal.startInit('10165f69-722d-4db7-b993-22179b1bf8ce', '817873775854');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      console.log('notificacion recibida',noti);
      
    });

    this.oneSignal.handleNotificationOpened().subscribe((noti) => {
      console.log('notificacion abierta',noti.notification.payload.additionalData);
      
    });

    //obtener id
    this.oneSignal.getIds().then(
      (info)=>{
        this.userId=info.userId;
        //this.presenters.presentAlert('Mensaje','UserId: '+info.userId);
        //guardo en el storage
        this.storage.get('onesignal_id').then(
          (onesignal_id)=>{
            if(onesignal_id==null){
              this.storage.set('onesignal_id',info.userId);
              this.storage.set('onesignal_guardado_DB',false);
            }
          }
        );
      }
    ).catch(
      (error)=>{
        console.log('Error al obtener el userId de onesignal',error);
      }
    );
    
    
    this.oneSignal.endInit();
  }
}
