import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { ToastController } from '@ionic/angular';

export enum ConnectionStatusEnum {
  Online,
  Offline
}

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  previousStatus;
  firstTime = false;
  constructor(public toast: ToastController, 
    public network: Network) { 
      this.previousStatus = ConnectionStatusEnum.Online;
    }

     detectNetwork(){
      console.log('hola desde network provider');
      
      this.network.onConnect().subscribe(data => {    
        if (!this.firstTime && this.previousStatus == 0){
          //nada
        }
        
        this.previousStatus = 1;
      }, error => console.error(error));
     
      this.network.onDisconnect().subscribe(data => {
        if (this.previousStatus == 1 || this.previousStatus == null){
          this.checkNetwork();
        }
  
        this.previousStatus = 0;
      }, error => console.error(error));
    }
  
    checkNetwork(){
        this.toast.create({
          message: 'Sin conexion a internet.',
          duration: 3000,
          position: 'top'
        });
    }
}
