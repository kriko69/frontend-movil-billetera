
import { Injectable } from '@angular/core';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { EstudianteServiceService } from '../estudiante-service.service';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class PresentersServiceService {

  loading:any;
  isLoading = false;
  constructor(public alerta:AlertController,
    private loadingCtrl:LoadingController,
    private eservice:EstudianteServiceService,
    private navController:NavController,
    private storage:Storage) { }

  async presentErrorAlert() {
    const alert = await this.alerta.create({
      header: 'Error',
      message: 'Ocurrio un error. Intente de nuevo por favor.',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentAlert(title:string,message:string) {
    const alert = await this.alerta.create({
      header: title,
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentAlertLogin(title:string,message:string) {
  
    const alert = await this.alerta.create({
        header: title,
        message: message,
        buttons: [
          {
            text: 'Entendido',
            handler: () => {
              this.presentLoadingLogin();
            }
          }
        ]
      });
      await alert.present();
  }

  async presentLoading(mensaje:string) {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      message:mensaje,
      backdropDismiss:true
    }).then(a => {
      a.present().then(() => {
        //console.log('presented');
        if (!this.isLoading) {
          a.dismiss();
        }
      });
    });
  }

  async presentLoadingLogin() {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      message:'Espere',
    }).then(a => {
      a.present().then(() => {
        //console.log('presented');
        if (!this.isLoading) {
          a.dismiss();
        }
      });
    });
  }


  async quitLoading() {
    this.isLoading = false;
    return await this.loadingCtrl.dismiss();
  }

  async alertaPasswordDefault(token) {
    let pass;
    let info:any;
    const alert = await this.alerta.create({
      header: 'Cambie su password por defecto',
      inputs: [
        {
          name: 'password1',
          type: 'text',
          placeholder: 'Password'
        },
        {
          name: 'password2',
          type: 'text',
          placeholder: 'Repita password'
        },
       
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: async data => {
            console.log(JSON.stringify(data)); //to see the object
            console.log(data.password1);
            console.log(data.password2);
            pass=data.password1;
            if(data.password1!=data.password2)
            {
              const alert = await this.alerta.create({
                header: 'Error',
                message: 'Las password no coinciden.',
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      this.alertaPasswordDefault(token);
                    }
                  } 
                ]
              });
              await alert.present();

              
            }else{
              console.log('password coinciden.');
              let es_valido=this.validarPassword(pass);
              if (es_valido) {
                this.presentLoading('Espere');
                this.eservice.cambiarPasswordDefault(token,pass).subscribe(
                  (data)=>{
                    this.quitLoading();
                    info=Object.assign(data);
                    console.log(info);
                    
                    if(info.mensaje=="el token ha expirado.")
                    {
                      console.log('token expiro',info);
                      this.storage.remove('token');
                      this.storage.remove('rol');
                      this.presentAlert("Error de inicio de sesion","La sesion expiro, inicie sesion nuevamente.");
                      this.navController.navigateRoot('/login');
                    }else{
                      if(info.estado=="exito")
                      {
                        this.presentAlert("Exito","La password se actualizo correctamente.");
                        
                      }else{
                        this.presentAlert("Error",info.mensaje);
                      }
                    }
                    
                  },(error)=>{
                    //cuando no hay servidor
                    this.quitLoading();
                    console.log('error al hacer la peticion');
                    this.storage.remove('token');
                    this.storage.remove('rol');
                    this.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
                    this.navController.navigateRoot('/login');
                    
                  }
                );
              } else {
                const alert = await this.alerta.create({
                  header: 'Error',
                  message: '<div>' +
                  '<p>El password no cumple con los requisitos requeridos.</p>' +
                  '<ul>'+
                    '<li>10 caracters.</li>'+
                    '<li>1 mayuscula.</li>'+
                    '<li>1 minuscula.</li>'+
                    '<li>1 numero.</li>'+
                    '<li>1 caracter especial.</li>'+
                    '(@$!%*?&)'+
                  '</ul>'+
                  '</div>',
                  buttons: [
                    {
                      text: 'Ok',
                      handler: () => {
                        this.alertaPasswordDefault(token);
                      }
                    } 
                  ]
                });
                await alert.present();
              }
              
            }
          }
        }
      ]
    });
    await alert.present();
  }

  validarPassword(password:string)
  {
    // se pone /expresion_regular/.test(palabra a examinar) y devuelve un booleano
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,}$/.test(password);
  }
}

