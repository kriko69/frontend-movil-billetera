import { TestBed } from '@angular/core/testing';

import { PresentersServiceService } from './presenters-service.service';

describe('PresentersServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PresentersServiceService = TestBed.get(PresentersServiceService);
    expect(service).toBeTruthy();
  });
});
