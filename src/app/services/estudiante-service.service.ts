import { estudiante } from './../../models/estudiante';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';
import { userLogin } from 'src/models/userLogin';
import { ParametersServiceService } from './parameters/parameters-service.service';
import { EncodingType } from '@ionic-native/camera/ngx';
import { CifradoHttpService } from './cifrado-http.service';
@Injectable({
  providedIn: 'root'
})
export class EstudianteServiceService {

  constructor(public http:HttpClient,
    public parameter:ParametersServiceService,
    public http2:Http,
    public cifrado:CifradoHttpService) { }

  login(us:userLogin){
    let data={
      "carnet":us.carnet,
      "password":us.password
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    opsi=this.cifrado.encriptacion(opsi);
    console.log('opsi',opsi);

    return this.http.post(this.parameter.ServidorUrl+'login', opsi, header);
  }

  perfil(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'perfil',{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerSaldo(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'usuario/saldo',{ headers:header}).pipe(map((res=>res.json())));
  }
  obtenerServicios(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'negocios/nombres',{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerTokenResetPassword(carnet:number)
  {
    let data={
      "carnet":carnet
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);

    return this.http.post(this.parameter.ServidorUrl+'reset/password', opsi, header);
  }

  verificarTokenResetPassword(carnet:number,codigo:string)
  {
    let data={
      "carnet":carnet,
      "codigo_reset_password":codigo
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);

    return this.http.post(this.parameter.ServidorUrl+'reset/password/verificar', opsi, header);
  }
  cambiarPassword(carnet:number,pass1:string,pass2:string)
  {
    let data={
      "password":pass1,
      "repetir_password":pass2,
      "carnet":carnet
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);

    return this.http.post(this.parameter.ServidorUrl+'reset/password/cambiar', opsi, header);
  }
  
  editarPerifl(token:string,estudiante:estudiante)
  {
    let data={
      "nombre":estudiante.nombre,
      "apellidos":estudiante.apellidos,
      "correo":estudiante.correo,
      "telefono":estudiante.telefono,
      "fecha_nacimiento":estudiante.fecha_con_formato
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'perfil',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerMisCuentas(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'usuario/mi/cuenta',{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerMovimientosDeMi(token:string,numero_cuenta:number)
  {
    let data={
      "numero_cuenta":numero_cuenta
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'usuario/movimientos/de/mi',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerMovimientosParaMi(token:string,numero_cuenta:number)
  {
    let data={
      "numero_cuenta":numero_cuenta
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'usuario/movimientos/para/mi',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerDatosTransferencia(token:string,numero_cuenta:number)
  {
    let data={
      "numero_cuenta":numero_cuenta
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'usuario/transferir/datos',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerCodigoTransferencia(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'usuario/transferir/codigo',{ headers:header}).pipe(map((res=>res.json())));
  }

  transferir(token:string,numero_cuenta:number,monto:number)
  {
    let data={
      "numero_cuenta":numero_cuenta,
      "monto":monto
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'usuario/transferir',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerDatosNegocioPago(token:string,negocio_id:number)
  {
    let data={
      "negocio_id":negocio_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'negocio/estudiante',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerDatosParaQr(token:string,negocio_id:number)
  {
    let data={
      "negocio_id":negocio_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'negocio/info/qr',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  registroNegocio(token:string,negocio:any)
  {
    let data={
      "nombre":negocio.nombre,
      "descripcion":negocio.descripcion,
      "hora_inicio":negocio.hora_inicio,
      "hora_fin":negocio.hora_fin
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'negocio',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  misNegocios(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'negocio/proveedor ',{ headers:header}).pipe(map((res=>res.json())));
  }

  actualizarNegocio(token:string,negocio:any)
  {
    let data={
      "nombre":negocio.nombre,
      "descripcion":negocio.descripcion,
      "hora_inicio":negocio.hora_inicio,
      "hora_fin":negocio.hora_fin,
      "negocio_id":negocio.negocio_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'negocio',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  pagar(token:string,dataPago:any)
  {
    let data={
      'negocio_id' : dataPago.negocio_id,
      'proveedor_id' : dataPago.proveedor_id,
      'carnet_dueno' : dataPago.carnet_dueno,
      'nombre_negocio' : dataPago.nombre_negocio,
      'numero_cuenta' : dataPago.numero_cuenta,
      'monto' : dataPago.monto
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'usuario/pagar',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  cambiarPasswordDefault(token:string,pass1:string,)
  {
    let data={
      "password":pass1,
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'usuario/password/default',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  reintentarAfiliacion(token:string,negocio_id:number)
  {
    let data={
      "negocio_id":negocio_id,
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'usuario/reintentar/afiliacion',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
  actualizarFotoPerfil(token:string,imagen:string,rol:string)
  {
    let data={
      "imagen":imagen,
      "rol":rol
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'usuario/foto',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
  eliminarFotoPerfil(token:string)
  {
    let data={
      "imagen":'sin imagen'
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'usuario/foto/eliminar',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  guardarOneSignalId(token:string,onesignal_id:string)
  {
    let data={
      "onesignal_id":onesignal_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'usuario/onesignal',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerGestiones(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'gestiones/estudiante',{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerMensualidadesNoPagadas(token:string,gestion_id:number)
  {
    let data={
      "gestion_id":gestion_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'mensualidades/estudiante/nopagadas',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
  obtenerMensualidadesPagadas(token:string,gestion_id:number)
  {
    let data={
      "gestion_id":gestion_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'mensualidades/estudiante/pagadas',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  pagarMensualidad(token:string,pago:any)
  {
    let data={
      "mensualidad_id":pago.mensualidad_id,
      "multa":pago.multa,
      "total":pago.total
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'mensualidades/estudiante/pagar',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  resetOneSignal(token:string)
  {
    let data={
      
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'usuario/onesignal/reset',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  obtenerOneSignalId(token:string,usuario2_id:any)
  {
    let data={
      "usuario_id":usuario2_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.post(this.parameter.ServidorUrl+'usuario/obtener/onesignal',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
  
  enviarNotificacionPushTransferencia(onesignal_id:string,nombre:string,monto)
  {
    let data={
      "app_id":"10165f69-722d-4db7-b993-22179b1bf8ce",
      "include_player_ids":[onesignal_id],
      "contents":{"es":"Recibiste una transferencia de "+nombre+" por un monto de "+monto+" Bs.","en":"You received a money transfer from "+nombre+" in the amount of "+monto+" Bs."},
      "headings":{"es":"Transferencia de dinero billetera movil","en":"Mobile wallet - Money transfer"}
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",'Basic YTA1MDlmMDYtMTg1Mi00Mzk5LThlMmMtNDU2ZWQ5YmQzOTNl'); 
    header.append("Content-Type","application/json");
    return this.http2.post('https://onesignal.com/api/v1/notifications',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  enviarNotificacionPushPago(onesignal_id:string,nombre:string,monto)
  {
    let data={
      "app_id":"10165f69-722d-4db7-b993-22179b1bf8ce",
      "include_player_ids":[onesignal_id],
      "contents":{"es":"Recibiste un pago de "+nombre+" por un monto de "+monto+" Bs.","en":"You received a pay from "+nombre+" in the amount of "+monto+" Bs."},
      "headings":{"es":"Pago de producto/servicio billetera movil","en":"Mobile wallet - Pay"}
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",'Basic YTA1MDlmMDYtMTg1Mi00Mzk5LThlMmMtNDU2ZWQ5YmQzOTNl'); 
    header.append("Content-Type","application/json");
    return this.http2.post('https://onesignal.com/api/v1/notifications',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
}


