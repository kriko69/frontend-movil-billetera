import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn:'root'
})
export class DataService{

    constructor(public http:HttpClient){

    }

    getMenuOps()
    {
        return this.http.get('../../assets/data/menu.json');
    }
}