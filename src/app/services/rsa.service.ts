import { Injectable } from '@angular/core';
import * as forge from 'node-forge';

@Injectable({
  providedIn: 'root'
})
export class RsaService {

  constructor() {

  }

  async createKeyPair():Promise<any>{
    const rsa = forge.pki.rsa;
    return new Promise((resolve,reject)=>{
      rsa.generateKeyPair(2048, async function(err, keypair) {
        const priv = keypair.privateKey;
        const pub = keypair.publicKey;
        // PEM serialize: public key
        const pubPem  = forge.pki.publicKeyToPem(pub);
        console.log("Public Key PEM:", pubPem);
        const pub2 = forge.pki.publicKeyFromPem(pubPem);
  
        // PEM serialize: private key
        const privPem  = forge.pki.privateKeyToPem(priv);
        console.log("Private Key PEM:", privPem);
        const priv2 = forge.pki.privateKeyFromPem(privPem);
        resolve({privPem,pubPem});
      });
    });
    
  }

  encriptar(publicKey,info)
  {
    //privateKey and publicKey estan en string
    const aa = forge.pki;
    let pub=aa.publicKeyFromPem(publicKey);

    const encrypted = pub.encrypt(info);
      
    return forge.util.encode64(encrypted);
      
  }

  desencriptar(privateKey,encrypted)
  {
    //privateKey and publicKey estan en string
    const aa = forge.pki;
    let pk=aa.privateKeyFromPem(privateKey);
    encrypted=forge.util.decode64(encrypted);
    return pk.decrypt(encrypted);
  }
}
