import { TestBed } from '@angular/core/testing';

import { CifradoNativoService } from './cifrado-nativo.service';

describe('CifradoNativoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CifradoNativoService = TestBed.get(CifradoNativoService);
    expect(service).toBeTruthy();
  });
});
