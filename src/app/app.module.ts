import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import{HttpClientModule} from '@angular/common/http';
import{HttpModule} from '@angular/http';

import {IonicStorageModule} from '@ionic/storage';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import {Base64ToGallery} from '@ionic-native/base64-to-gallery/ngx'
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import{AndroidFingerprintAuth} from '@ionic-native/android-fingerprint-auth/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Network } from '@ionic-native/network/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { AES256 } from '@ionic-native/aes-256/ngx';

import { SumarFechaPipe } from './pipes/sumar-fecha.pipe';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    NgxQRCodeModule,
    IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    Base64ToGallery,
    BarcodeScanner,
    AndroidPermissions,
    AndroidFingerprintAuth,
    LocalNotifications,
    Network,
    OneSignal,
    Camera,
    AES256,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
