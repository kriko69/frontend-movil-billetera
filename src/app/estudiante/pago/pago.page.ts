import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController } from '@ionic/angular';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import {Storage} from '@ionic/storage';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { stringify } from '@angular/compiler/src/util';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.page.html',
  styleUrls: ['./pago.page.scss'],
})
export class PagoPage implements OnInit {

  pago={
    monto:''
  };
  negocio_id:number;
  datosNegocio={
    "nombre":"",
    "apellidos":"",
    "nombre_negocio":"",
    "descripcion":"",
    "telefono":''
  };
  qrData:string;
  cantidadPipes=0;
  nombre_negocio;
  proveedor_id;
  user;
  constructor(public alertController:AlertController,
    public menucontroller:MenuController,
    public navController:NavController,
    public eservice:EstudianteServiceService,
    public storage:Storage,
    public presenters:PresentersServiceService,
    public parameters:ParametersServiceService,
    private localNotifications: LocalNotifications,
    public encrypt:CifradoNativoService) { 
    this.menucontroller.enable(false,'first');
  }

  ngOnInit() {
    //this.presenters.presentLoading('Espere');
    this.qrData=this.parameters.getQrData();
    this.qrData=this.encrypt.desencriptar(this.qrData);
    this.nombre_negocio=this.parameters.getNombreNegocioPago();
    let qrDataSplit=this.qrData.split('||');
    for (let i = 0; i < this.qrData.length; i++) {
      if(this.qrData.charAt(i)=="|")
      {
        this.cantidadPipes=this.cantidadPipes+1;
      } 
    }
    if(this.cantidadPipes==8 && qrDataSplit[4]==this.nombre_negocio)
    {
      
      this.presentAlert();
      //this.presenters.quitLoading();
      this.negocio_id=this.parameters.getNegocioId();
      console.log('negocio_id desde pago',this.negocio_id);
      this.obtenerDatosNegocio(this.negocio_id);

    }else{
      this.presenters.presentAlert('Error','Codigo QR invalido.');
      //this.presenters.quitLoading();
      this.navController.navigateRoot('/negocios-pago');
    }
    
    
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Codigo QR escaneado con exito',
      message: '<img src="../../../assets/check.png">',
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentAlertPago() {
    const alert = await this.alertController.create({
      header: 'Tu pago se realizo con exito',
      message: '<img src="../../../assets/pago-exito.png">',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navController.navigateRoot('/login')
          }
        }
      ]
    });

    await alert.present();
  }

  pagar()
  {
    console.log('monto',parseInt(this.pago.monto));
    
    if(parseInt(this.pago.monto)>0)
    {
      console.log(this.pago);
      let qrDataSplit=this.qrData.split('||');
      this.proveedor_id=parseInt(qrDataSplit[1])
      console.log(qrDataSplit);
      let dataPago={
        "negocio_id":this.negocio_id,
        "proveedor_id":parseInt(qrDataSplit[1]),
        "carnet_dueno":parseInt(qrDataSplit[2]),
        "nombre_negocio":qrDataSplit[4],
        "numero_cuenta":parseInt(qrDataSplit[3]),
        "monto":parseInt(this.pago.monto)
      };
      console.log('dataPago',dataPago);
      this.realizarPago(dataPago);
    }else{
      this.presenters.presentAlert('Error','El monto debe ser mayor a 0.')
    }
  }

  obtenerDatosNegocio(negocio_id:number)
  {
    let info:any;
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.obtenerDatosNegocioPago(tokenDes,negocio_id).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              if(info.estado=="exito")
              {
                console.log('info.data[0]',info.data[0]);
                this.datosNegocio.nombre=info.data[0].nombre;
                this.datosNegocio.apellidos=info.data[0].apellidos;
                this.datosNegocio.nombre_negocio=info.data[0].nombre_negocio;
                this.datosNegocio.descripcion=info.data[0].descripcion;
                this.datosNegocio.telefono=info.data[0].telefono;
                
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                } else {
                  //hay un error
                  this.presenters.presentAlert('Error', info.mensaje);
                  this.navController.navigateRoot('/negocios-pago');
                }
              }
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.storage.remove('token');
              this.storage.remove('rol');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor.");
              this.navController.navigateRoot('/negocios-pago');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.quitLoading();
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
    this.presenters.quitLoading();
  }

  realizarPago(dataPago)
  {
    let info:any;
    this.presenters.presentLoading('Espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.pagar(tokenDes,dataPago).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              if(info.estado=="exito")
              {
                this.presenters.presentAlert('Exito','El pago se realizo con exito');
                this.navController.navigateRoot('menu-estudiante');
                this.mandarNotificacionLocal();
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                } else {
                  //hay un error
                  this.presenters.presentAlert('Error', info.mensaje);
                  this.navController.navigateRoot('/negocios-pago');
                }
              }
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.storage.remove('token');
              this.storage.remove('rol');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor.");
              this.navController.navigateRoot('/negocios-pago');
              
            }
          );
        }else{
          //no hay token
          this.presenters.quitLoading();
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  mandarNotificacionLocal()
  {
    setTimeout(()=>{
      this.localNotifications.schedule({
        id: 1,
        text: 'Realizaste un pago a '+this.datosNegocio.nombre_negocio+' con un valor de '+this.pago.monto+' Bs.',
        data: { page: 'tipo-movimientos' },
        icon:'../../../assets/wallet.jpg'
      }); 
    },3000);

    setTimeout(()=>{
      this.mandarNotificacionPush();
    },3000);
  }

  mandarNotificacionPush()
  {
    let usuario_id=this.proveedor_id;
    this.user=this.parameters.getPerfilLogin();
    let info:any;
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.obtenerOneSignalId(tokenDes,usuario_id).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              if(info.estado=="error")
              {
                //token expiro
                this.presenters.presentAlert("Error",info.mensaje);
              }else{
                console.log('token bien',info);
                let one_signal_id=info.onesignal_id;
                let nombre=this.user.nombre+" "+this.user.apellidos;
                let monto=this.pago.monto;
                console.log('onesignal id',one_signal_id);
                if(one_signal_id!='s/n')
                {
                  this.eservice.enviarNotificacionPushPago(one_signal_id,nombre,monto).subscribe(
                    (data)=>{
                      console.log('push enviada con exito');
                      
                    },(error)=>{
                      console.log('No se pudo enviar la push :(');
                    }
                  );
                  
                }
              }
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor.");
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.quitLoading();
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

}
