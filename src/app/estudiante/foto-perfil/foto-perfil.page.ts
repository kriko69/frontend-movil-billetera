import { Component, OnInit } from '@angular/core';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';
@Component({
  selector: 'app-foto-perfil',
  templateUrl: './foto-perfil.page.html',
  styleUrls: ['./foto-perfil.page.scss'],
})
export class FotoPerfilPage implements OnInit {

  linkFoto;
  nombreFoto;
  base64Image;
  fotoUsuario;
  rol;
  constructor(public parameters:ParametersServiceService,
    public presenters:PresentersServiceService,
    public storage:Storage,
    public navController:NavController,
    private camera: Camera,
    public eservice:EstudianteServiceService,
    public encrypt:CifradoNativoService) { 
      
    }

  ngOnInit() {
    this.mostrarMensaje();
    this.storage.get('rol').then(
      (rol)=>{
        let rolDes=this.encrypt.desencriptar(rol);
        console.log('rol',rolDes);
        this.rol=rolDes;
        if(rolDes!=null)
        {
          if(rolDes=="estudiante"){
            this.linkFoto=this.parameters.getimagenUsuarioEstudiante();
            this.nombreFoto=this.parameters.getNombreFotoPerfil();
            this.linkFoto=this.linkFoto+this.nombreFoto;
          }else{
            if(rolDes=="proveedor"){
              this.linkFoto=this.parameters.getimagenUsuarioProveedor();
              this.nombreFoto=this.parameters.getNombreFotoPerfil();
              this.linkFoto=this.linkFoto+this.nombreFoto;
            }else{
              this.presenters.presentAlert('Error del rol','Rol guardado incorrecto.');
              this.navController.navigateRoot('/login');
            }
          }
        }else{
          this.presenters.presentAlert('Error del rol','Inicie sesion nuevamente.');
          this.navController.navigateRoot('/login');
        }
      }
    );
    
    
  }

  galeria()
  {
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation:true,
      allowEdit:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = imageData; //'data:image/png;base64,'+ 
    this.fotoUsuario = this.base64Image;
    this.actulizarFoto(this.base64Image);
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  }

  camara()
  {
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL, // FILE_URI para android
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation:true,
      allowEdit:true
    }
    
    this.camera.getPicture(options).then((imageData)=> {
      this.base64Image =ImageData;
      this.fotoUsuario = this.base64Image;
      this.actulizarFoto(this.base64Image);
    },(err)=>{
      console.log('Error en la foto tomada')
    });
  }

  eliminar()
  {
    let info:any;
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.eliminarFotoPerfil(tokenDes).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              if(info.estado=="error")
              {
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                } else {
                  //hay un error
                  this.presenters.presentAlert('Error', info.mensaje);
                  this.navController.navigateRoot('/perfil');
                }
                
              }else{
                console.log('token bien',info);
                this.presenters.presentAlert('Exito','Se elimino la foto de perfil correctamente.');
                this.navController.navigateRoot('/perfil');
              }
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.storage.remove('token');
              this.storage.remove('rol');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
              this.navController.navigateRoot('/perfil');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.quitLoading();
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }
  actulizarFoto(imagen)
  {
    console.log(imagen);
    let info:any;
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.actualizarFotoPerfil(tokenDes,imagen,this.rol).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              if(info.estado=="error")
              {
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                } else {
                  //hay un error
                  this.presenters.presentAlert('Error', info.mensaje);
                  this.navController.navigateRoot('/perfil');
                }
                
              }else{
                console.log('token bien',info);
                this.presenters.presentAlert('Exito','Se actualizo la foto de perfil correctamente.');
                this.navController.navigateRoot('/perfil');
              }
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
              this.navController.navigateRoot('/perfil');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.quitLoading();
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
    
  }

  mostrarMensaje()
  {
    this.presenters.presentAlert('Aviso','Las imagenes mayores a 3 Mb de tamaño puede tardar en cargar.');
  }

}
