import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { Storage } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-transferencia',
  templateUrl: './transferencia.page.html',
  styleUrls: ['./transferencia.page.scss'],
})
export class TransferenciaPage implements OnInit {

  mostrarDatos=false;
  codigo:string;
  codigoToggle=false;
  infoCuenta:any;
  disableCuenta=false;
  disableMonto=false;
  checkedToggle=false;
  transferencia={
    numero_cuenta:'',
    monto:'',
    codigo:''
  }
  perfil;
  constructor(public storage:Storage,
    public navController:NavController,
    public eservice:EstudianteServiceService,
    public parameters:ParametersServiceService,
    public menucontroller:MenuController,
    public presenters:PresentersServiceService,
    public localNotifications:LocalNotifications,
    public encrypt:CifradoNativoService) {

   }

  ngOnInit() {
    console.log('perfil',this.parameters.getPerfilLogin());
    this.perfil=this.parameters.getPerfilLogin();
  }

  mostrar()
  {
    if(this.transferencia.numero_cuenta!=null){
    let info:any;
    this.presenters.presentLoading("Espere");
    console.log(this.transferencia);
    let numero_cuenta=parseInt(this.transferencia.numero_cuenta);
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if(tokenDes!=null)
        {
          //hay token
          this.eservice.obtenerDatosTransferencia(tokenDes,numero_cuenta).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log('info',info);
              if(info.estado=="exito"){
                this.infoCuenta=Object.assign(info.data[0]);
                console.log('info cuenta',this.infoCuenta);
                this.mostrarDatos=true;
                this.disableCuenta=true;
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                } else {
                  //hay un error
                  this.presenters.presentAlert('Error', info.mensaje);
                  this.navController.navigateRoot('/transferencia');
                }
              }
            },(error)=>{
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
              this.navController.navigateRoot('/transferencia');
            }
          );
        }else{
          //no hay token
          this.presenters.quitLoading();
          console.log('no hay token');
          this.storage.remove('token');
          this.storage.remove('rol');
          this.presenters.presentAlert("Error en el token","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
    }else{
      this.presenters.presentAlert("Error","El numero de cuenta no puede ser nulo.");
      this.navController.navigateRoot('/transferencia');
    }
  }
  cerrar()
  {
    this.mostrarDatos=false;
  }

  generarCodigo()
  {
    this.codigoToggle = !this.codigoToggle;
    if (this.codigoToggle) {
      let info:any;
      this.presenters.presentLoading('espere');
      this.storage.get('token').then(
        (token)=>{
          let tokenDes=this.encrypt.desencriptar(token);
          if(tokenDes!=null)
          {
            //hay token
            this.eservice.obtenerCodigoTransferencia(tokenDes).subscribe(
              (data)=>{
                this.presenters.quitLoading();
                info=Object.assign(data);
                console.log('info',info);
                if(info.estado=="exito"){
                  
                  this.codigo = info.data;
                  this.presenters.presentAlert('Exito',info.mensaje);
                }else{
                  if(info.mensaje=="el token ha expirado.")
                  {
                    //ha expirado el token
                    this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                    this.storage.remove('token');
                    this.storage.remove('rol');
                    this.navController.navigateRoot('/login');
                  } else {
                    //hay un error
                    this.presenters.presentAlert('Error', info.mensaje);
                    this.navController.navigateRoot('/transferencia');
                  }
                }
              },(error)=>{
                this.presenters.quitLoading();
                console.log('error al hacer la peticion');
                this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
                this.navController.navigateRoot('/transferencia');
              }
            );
          }else{
            //no hay token
            this.presenters.quitLoading();
            console.log('no hay token');
            this.storage.remove('token');
            this.storage.remove('rol');
            this.presenters.presentAlert("Error en el token","Token no disponible, inicie sesion nuevamente.");
            this.navController.navigateRoot('/login');
          }
        }
      );
      
    } else {
      this.codigo = "";
    }  
  }

  transferir()
  {
    console.log(this.transferencia);
    if(this.transferencia.monto!=null)
    {
      if(parseInt(this.transferencia.monto)>0)
      {
        //es diferente de null y mayor a 0
        if(this.transferencia.codigo!=null){
          if(this.transferencia.codigo==this.codigo){
            console.log('se transfiere');
            console.log(this.transferencia);

            let info: any;
            this.presenters.presentLoading('espere');
            this.storage.get('token').then(
              (token) => {
                let tokenDes=this.encrypt.desencriptar(token);
                if (tokenDes != null) {
                  //hay token
                  this.eservice.transferir(tokenDes,parseInt(this.transferencia.numero_cuenta),parseInt(this.transferencia.monto)).subscribe(
                    (data) => {
                      this.presenters.quitLoading();
                      info = Object.assign(data);
                      console.log('info', info);
                      if (info.estado == "exito") {

                        this.presenters.presentAlert('Exito','Se realizo la transaccion correctamente.');
                        this.navController.navigateRoot('/menu-estudiante');
                        this.mandarNotificacionLocal();
                      } else {
                        if (info.mensaje == "el token ha expirado.") {
                          //ha expirado el token
                          this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                          this.storage.remove('token');
                          this.storage.remove('rol');
                          this.navController.navigateRoot('/login');
                        } else {
                          //hay un error
                          this.presenters.presentAlert('Error', info.mensaje);
                          this.navController.navigateRoot('/transferencia');
                        }
                      }
                    }, (error) => {
                      this.presenters.quitLoading();
                      console.log('error al hacer la peticion');
                      this.presenters.presentAlert("Error", "No se pudo contactar al servidor, intente mas tarde.");
                      this.navController.navigateRoot('/transferencia');
                    }
                  );
                } else {
                  //no hay token
                  this.presenters.quitLoading();
                  console.log('no hay token');
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.presenters.presentAlert("Error en el token", "Token no disponible, inicie sesion nuevamente.");
                  this.navController.navigateRoot('/login');
                }
              }
            );

          }else{
            //codigos no iguales
            this.presenters.presentAlert('Error','Los codigos de verificacion no coinciden.');
          }
        }else{
          //codigo es null
          this.presenters.presentAlert('Error','Debe ingresar el codigo de verificacion.');
        }
        
      }else{
        //es diferente de null pero 0
        this.presenters.presentAlert('Error','El monto debe ser mayor a 0.');
      }
    }else{
      //es null
      this.presenters.presentAlert('Error','Debe ingresar algun monto.');
    }
    
  }

  mandarNotificacionLocal()
  {
    setTimeout(()=>{
      this.localNotifications.schedule({
        id: 1,
        text: 'Realizaste una transferencia a '+this.infoCuenta.nombre+' '+this.infoCuenta.apellidos+' con un valor de '+this.transferencia.monto+' Bs.',
        data: { page: 'tipo-movimientos' },
        icon:'../../../assets/wallet.jpg'
      }); 
    },3000);
    
    setTimeout(()=>{
      this.mandarNotificacionPush();
    },3000);
  }

  mandarNotificacionPush()
  {
    let usuario_id=this.infoCuenta.usuario_id;
    let info:any;
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.obtenerOneSignalId(tokenDes,usuario_id).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              if(info.estado=="error")
              {
                //token expiro
                console.log('token expiro',info);
                this.storage.remove('token');
                this.storage.remove('rol');
                this.presenters.presentAlert("Error de inicio de sesion","La sesion expiro, inicie sesion nuevamente.");
                this.navController.navigateRoot('/login');
              }else{
                console.log('token bien',info);
                let one_signal_id=info.onesignal_id;
                let nombre=this.perfil.nombre+" "+this.perfil.apellidos;
                let monto=this.transferencia.monto;
                console.log('onesignal id',one_signal_id);
                if(one_signal_id!='s/n')
                {
                  this.eservice.enviarNotificacionPushTransferencia(one_signal_id,nombre,monto).subscribe(
                    (data)=>{
                      console.log('push enviada con exito');
                      
                    },(error)=>{
                      console.log('No se pudo enviar la push :(');
                    }
                  );
                }
              }
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.storage.remove('token');
              this.storage.remove('rol');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
              this.navController.navigateRoot('/login');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.quitLoading();
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

}
