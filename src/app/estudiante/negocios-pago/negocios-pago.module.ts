import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NegociosPagoPageRoutingModule } from './negocios-pago-routing.module';

import { NegociosPagoPage } from './negocios-pago.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NegociosPagoPageRoutingModule
  ],
  declarations: [NegociosPagoPage]
})
export class NegociosPagoPageModule {}
