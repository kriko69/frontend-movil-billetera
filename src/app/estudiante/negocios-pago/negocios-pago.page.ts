import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-negocios-pago',
  templateUrl: './negocios-pago.page.html',
  styleUrls: ['./negocios-pago.page.scss'],
})
export class NegociosPagoPage implements OnInit {

  info:any;
  negocios:any;
  cantidad:any;
  constructor(public storage:Storage,
    public navController:NavController,
    public eservice:EstudianteServiceService,
    public parameters:ParametersServiceService,
    public presenters:PresentersServiceService,
    public barCode:BarcodeScanner,
    public encrypt:CifradoNativoService) { 

  }

  ngOnInit() {
    this.obtenerNegocios();
    console.log(this.parameters.getPerfilLogin());

  }

  irCamara(negocio_id,nombre_negocio)
  {
    
    console.log('negocio_id',negocio_id);
    this.parameters.setNegocioId(negocio_id);
    this.parameters.setNombreNegocioPago(nombre_negocio);
    this.barCode.scan().then(
      (data)=>{
        if(data.cancelled)
        {
          this.navController.navigateRoot('/negocios-pago');
        }else{
          this.parameters.setQrData(data.text);
          this.navController.navigateRoot('/pago');
        }
        
      },(error)=>{
        this.presenters.presentAlert('Error','No se pudo abrir la camara.');
      }
    );

    
  }

  obtenerNegocios()
  {
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          //hay token
          this.eservice.obtenerServicios(tokenDes).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              this.info=Object.assign(data);
              console.log(this.info);
              if(this.info.mensaje=="el token ha expirado.")
              {
                console.log('token expiro',this.info);
                this.storage.remove('token');
                this.storage.remove('rol');
                this.presenters.presentAlert("Error","La sesion expiro, inicie sesion nuevamente.");
                this.navController.navigateRoot('/login');
              }else{
                if(this.info.estado=="exito" && this.info.data==null)
                {
                  this.info.data=[];
                  this.negocios=this.info.data;
                  this.cantidad=0;
                  console.log('negocios',this.info["data"].length);
                  this.presenters.presentAlert("Aviso de servicios",this.info.descripcion);
                  
                }else{
                  this.negocios=this.info.data;
                  this.cantidad=this.negocios.length;
                  console.log('negocios',this.negocios.length);
                }
              }
              
            },(error)=>{
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor.");
            }
          );
        }else{
          //no hay token
          this.presenters.quitLoading();
          console.log('no hay token');
          this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

}
