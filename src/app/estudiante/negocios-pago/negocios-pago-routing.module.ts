import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NegociosPagoPage } from './negocios-pago.page';

const routes: Routes = [
  {
    path: '',
    component: NegociosPagoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NegociosPagoPageRoutingModule {}
