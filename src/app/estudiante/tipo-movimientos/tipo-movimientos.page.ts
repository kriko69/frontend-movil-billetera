import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';

@Component({
  selector: 'app-tipo-movimientos',
  templateUrl: './tipo-movimientos.page.html',
  styleUrls: ['./tipo-movimientos.page.scss'],
})
export class TipoMovimientosPage implements OnInit {

  soyProveedor:boolean;
  ruta:string;
  constructor(public navController:NavController,
    public parameters:ParametersServiceService) {
        this.soyProveedor=this.parameters.getSoyProveedor();
        if(this.soyProveedor)
        {
          this.ruta='/cuentas';
          //this.parameters.setSoyProveedor(false);
        }else{
          this.ruta='/menu-estudiante';
        }
     }

  ngOnInit() {
    
  }

  deMi()
  {
    console.log('de mi');
    this.parameters.setTipoMovimiento('de mi');
    if(this.soyProveedor)
    {
      this.navController.navigateRoot('/movimientos-proveedor');
    }else{

      this.navController.navigateRoot('/movimientos');
    }
  }

  paraMi()
  {
    console.log('para mi');
    this.parameters.setTipoMovimiento('para mi');
    if(this.soyProveedor)
    {
      this.navController.navigateRoot('/movimientos-proveedor');
    }else{
      
      this.navController.navigateRoot('/movimientos');
    }
    
  }

}
