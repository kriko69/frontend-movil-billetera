import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoMovimientosPage } from './tipo-movimientos.page';

const routes: Routes = [
  {
    path: '',
    component: TipoMovimientosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TipoMovimientosPageRoutingModule {}
