import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TipoMovimientosPageRoutingModule } from './tipo-movimientos-routing.module';

import { TipoMovimientosPage } from './tipo-movimientos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TipoMovimientosPageRoutingModule
  ],
  declarations: [TipoMovimientosPage]
})
export class TipoMovimientosPageModule {}
