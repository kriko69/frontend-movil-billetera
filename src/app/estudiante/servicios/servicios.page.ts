import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import {Storage} from '@ionic/storage'
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';
@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.page.html',
  styleUrls: ['./servicios.page.scss'],
})
export class ServiciosPage implements OnInit {

  info:any;
  negocios:any;
  cantidad:number;
  constructor(public menucontroller:MenuController,
    public eservice:EstudianteServiceService,
    public storage:Storage,
    public presenters:PresentersServiceService,
    public navController:NavController,
    public encrypt:CifradoNativoService) {
    
    this.menucontroller.enable(false,'first');
   }

  ngOnInit() {
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          //hay token
          this.eservice.obtenerServicios(tokenDes).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              this.info=Object.assign(data);
              console.log(this.info);
              if(this.info.mensaje=="el token ha expirado.")
              {
                console.log('token expiro',this.info);
                this.storage.remove('token');
                this.storage.remove('rol');
                this.presenters.presentAlert("Error de inicio de sesion","La sesion expiro, inicie sesion nuevamente.");
                this.navController.navigateRoot('/login');
              }else{
                if(this.info.estado=="exito" && this.info.data==null)
                {
                  this.info.data=[];
                  this.negocios=this.info.data;
                  this.cantidad=0;
                  console.log('negocios',this.info["data"].length);
                  this.presenters.presentAlert("Aviso de servicios",this.info.descripcion);
                  
                }else{
                  this.negocios=this.info.data;
                  this.cantidad=this.negocios.length;
                  console.log('negocios',this.negocios.length);
                }
              }
              
            },(error)=>{
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
            }
          );
        }else{
          //no hay token
          this.presenters.quitLoading();
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  

}
