import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { Storage } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-pago-mensualidad',
  templateUrl: './pago-mensualidad.page.html',
  styleUrls: ['./pago-mensualidad.page.scss'],
})
export class PagoMensualidadPage implements OnInit {

  gestion;
  mensualidad;
  total=0;
  multa=0;
  diasMulta;
  codigo:string;
  codigoToggle=false;
  codigoIngresado:string;
  pago={
    mensualidad_id:0,
    multa:0,
    total:0
  }
  constructor(public storage:Storage,
    public navController:NavController,
    public eservice:EstudianteServiceService,
    public parameters:ParametersServiceService,
    public menucontroller:MenuController,
    public presenters:PresentersServiceService,
    public localNotifications:LocalNotifications,
    public encrypt:CifradoNativoService) {

   }

  ngOnInit() {
    this.gestion=this.parameters.getGestion();
    this.mensualidad=this.parameters.getMensaulidad();
    console.log(this.gestion);
    console.log(this.mensualidad);
    this.calcularMulta();
    
  }

  calcularMulta()
  {
    let date = new Date()

    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getFullYear()
    console.log(day+'-'+month+'-'+year);

    let mensualidad_date=this.mensualidad.fecha_pago;
    let split =mensualidad_date.split('-');
    let dia,mes,ano;
    dia=+split[0];
    mes=+split[1];
    ano=+split[2]; 

    let diasActual=this.convertirADias(day,month,year);
    let diasMensualidad=this.convertirADias(dia,mes,ano);
    console.log('dias actual',diasActual);
    console.log('dias mensualidad',diasMensualidad);
    let diasMulta;
    if(diasActual>diasMensualidad)
    {
      diasMulta=diasActual-diasMensualidad;
      this.diasMulta=diasMulta;
      console.log('dias multa',diasMulta);
      this.multa=diasMulta*0.2;
      this.multa=Math.round(this.multa * 100) / 100;
      this.multa=parseFloat(this.multa.toFixed(2));
      this.total=parseFloat(this.multa.toFixed(2))+this.mensualidad.monto;
      this.total=Math.round(this.total * 100) / 100;
      this.total=parseFloat(this.total.toFixed(2));
    } else{
      this.diasMulta=0;
      this.total=this.mensualidad.monto;
    }
   
  }

  convertirADias(dia,mes,ano)
  {
    let totalDias=0;
    totalDias=totalDias+dia;
    totalDias=totalDias+(mes*30);
    totalDias=totalDias+(ano*365);
    return totalDias;
  }

  generarCodigo()
  {
    this.codigoToggle = !this.codigoToggle;
    if (this.codigoToggle) {
      let info:any;
      this.presenters.presentLoading('espere');
      this.storage.get('token').then(
        (token)=>{
          let tokenDes=this.encrypt.desencriptar(token);
          if(tokenDes!=null)
          {
            //hay token
            this.eservice.obtenerCodigoTransferencia(tokenDes).subscribe(
              (data)=>{
                this.presenters.quitLoading();
                info=Object.assign(data);
                console.log('info',info);
                if(info.estado=="exito"){
                  
                  this.codigo = info.data;
                  this.presenters.presentAlert('Exito',info.mensaje);
                }else{
                  if(info.mensaje=="el token ha expirado.")
                  {
                    //ha expirado el token
                    this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                    this.storage.remove('token');
                    this.storage.remove('rol');
                    this.navController.navigateRoot('/login');
                  } else {
                    //hay un error
                    this.presenters.presentAlert('Error', info.mensaje);
                    this.navController.navigateRoot('/transferencia');
                  }
                }
              },(error)=>{
                this.presenters.quitLoading();
                console.log('error al hacer la peticion');
                this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
                this.navController.navigateRoot('/transferencia');
              }
            );
          }else{
            //no hay token
            this.presenters.quitLoading();
            console.log('no hay token');
            this.storage.remove('token');
            this.storage.remove('rol');
            this.presenters.presentAlert("Error en el token","Token no disponible, inicie sesion nuevamente.");
            this.navController.navigateRoot('/login');
          }
        }
      );
      
    } else {
      this.codigo = "";
    }  
  }

  pagar()
  {
    if(this.codigoIngresado!=null){
      if(this.codigoIngresado==this.codigo){
        console.log('se paga');
        this.pago.mensualidad_id=this.mensualidad.mensualidad_id;
        this.pago.multa=this.multa;
        this.pago.total=this.total;
        console.log(this.pago);
        this.realizarPago();

      }else{
        this.presenters.presentAlert('Error','Los codigos de verificacion no coinciden.');
      }
    }else{
      this.presenters.presentAlert('Error','Debe ingresar el codigo de verificacion.');
    }
  }

  realizarPago()
  {
    let info:any;
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.pagarMensualidad(tokenDes,this.pago).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log(info);
              
              if(info.estado=="exito")
              {
                this.presenters.presentAlert('Exito','Se realizo el pago de la mensuaidad correctamente.')
                this.navController.navigateRoot('/menu-estudiante');
                this.enviarNotificacionLocal();
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                } else {
                  //hay un error
                  this.presenters.presentAlert('Error', info.mensaje);
                  this.navController.navigateRoot('/tipo-mensualidad');
                }
              }
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.storage.remove('token');
              this.storage.remove('rol');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor.");
              this.navController.navigateRoot('/negocios-pago');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.quitLoading();
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  enviarNotificacionLocal()
  {
    setTimeout(()=>{
      this.localNotifications.schedule({
        id: 1,
        text: 'Realizaste el pago de tu mensualidad universitaria por un monto de'+this.total+'Bs. No olvides de pagar a tiempo para evitar una multa.',
        data: { page: 'tipo-movimientos' },
        icon:'../../../assets/wallet.jpg'
      }); 
    },3000);
  }

}
