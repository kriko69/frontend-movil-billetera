import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagoMensualidadPageRoutingModule } from './pago-mensualidad-routing.module';

import { PagoMensualidadPage } from './pago-mensualidad.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagoMensualidadPageRoutingModule
  ],
  declarations: [PagoMensualidadPage]
})
export class PagoMensualidadPageModule {}
