import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagoMensualidadPage } from './pago-mensualidad.page';

const routes: Routes = [
  {
    path: '',
    component: PagoMensualidadPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagoMensualidadPageRoutingModule {}
