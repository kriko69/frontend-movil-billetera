import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PagoMensualidadPage } from './pago-mensualidad.page';

describe('PagoMensualidadPage', () => {
  let component: PagoMensualidadPage;
  let fixture: ComponentFixture<PagoMensualidadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoMensualidadPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PagoMensualidadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
