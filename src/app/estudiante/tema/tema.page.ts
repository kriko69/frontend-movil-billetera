import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { NavController } from '@ionic/angular';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-tema',
  templateUrl: './tema.page.html',
  styleUrls: ['./tema.page.scss'],
})
export class TemaPage implements OnInit {

  ruta;
  darkMode:boolean;
  
  constructor(public storage:Storage,
    public presenters:PresentersServiceService,
    public navController:NavController,
    public encrypt:CifradoNativoService) {
      this.storage.get('dark').then(
        (dark)=>{
          this.ver(dark);
        }
      );
      
      
     }

  ngOnInit() {
    this.verificarRol();
  }

  ver(dark)
  {
    this.darkMode=dark;
    console.log(this.darkMode);
    
    
  }

  verificarRol()
  {
    this.storage.get('rol').then(
      (rol)=>{
        let rolDes=this.encrypt.desencriptar(rol);
        console.log('rol',rolDes);
        
        if(rolDes!=null)
        {
          if(rolDes=="estudiante"){
            this.ruta='menu-estudiante';
          }else{
            if(rolDes=="proveedor"){
              this.ruta='menu-proveedor';
            }else{
              this.presenters.presentAlert('Error del rol','Rol guardado incorrecto.');
              this.navController.navigateRoot('/login');
            }
          }
        }else{
          this.presenters.presentAlert('Error del rol','Inicie sesion nuevamente.');
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  cambiarTema()
  {
    
    document.body.classList.toggle('dark'); //cambio el tema
    this.darkMode=!this.darkMode; //cambio la variable tru si es dark y false si no lo es
    console.log('dark despues de cambiar',this.darkMode);
    this.storage.set('dark',this.darkMode); //seteo dark
    this.storage.get('dark').then((dark)=>{
      if(dark) //si es dark
      {
        this.storage.set('firstDark',true); //pongo true firstDark para que cuando inicie la aplicacion siga dark theme
        this.storage.set('imageTheme','logo-black.png');
      }else{
        this.storage.set('imageTheme','logo.png');
      }
    });
    
    //this.navController.navigateRoot(this.ruta);
    
  }

}
