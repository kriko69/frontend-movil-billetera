import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GestionesPage } from './gestiones.page';

const routes: Routes = [
  {
    path: '',
    component: GestionesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionesPageRoutingModule {}
