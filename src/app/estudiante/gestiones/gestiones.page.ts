import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import {Storage} from '@ionic/storage'
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';
@Component({
  selector: 'app-gestiones',
  templateUrl: './gestiones.page.html',
  styleUrls: ['./gestiones.page.scss'],
})
export class GestionesPage implements OnInit {

  gestiones;
  cantidad;
  mensualidadPagada;
  constructor(public menucontroller:MenuController,
    public eservice:EstudianteServiceService,
    public storage:Storage,
    public presenters:PresentersServiceService,
    public navController:NavController,
    public parameters:ParametersServiceService,
    public encrypt:CifradoNativoService) {
    
    this.menucontroller.enable(false,'first');
   }


  ngOnInit() {
    this.mensualidadPagada=this.parameters.getMmensualidadPagada();
    this.obtenerGestiones();
  }

  irMensualidades(gestion)
  {
    this.parameters.setGestion(gestion);
    if(this.mensualidadPagada)
    {
      this.navController.navigateRoot('/mensualidades-pagadas');
    }else{
      this.navController.navigateRoot('/mensualidades');
    }
    
  }

  obtenerGestiones() {
    let info;
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          //hay token
          this.eservice.obtenerGestiones(tokenDes).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log(info);
              
              if(info.mensaje=="el token ha expirado.")
              {
                console.log('token expiro',info);
                this.storage.remove('token');
                this.storage.remove('rol');
                this.presenters.presentAlert("Error de inicio de sesion","La sesion expiro, inicie sesion nuevamente.");
                this.navController.navigateRoot('/login');
              }else{
                if(info.estado=="exito" && info.data==null)
                {
                  info.data=[];
                  this.gestiones=info.data;
                  this.cantidad=0;
                  console.log('negocios',info["data"].length);
                  this.presenters.presentAlert("Aviso de gestiones",info.descripcion);
                  
                }else{
                  this.gestiones=info.data;
                  this.cantidad=this.gestiones.length;
                  console.log('negocios',this.gestiones.length);
                }
              }
              
            },(error)=>{
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
            }
          );
        }else{
          //no hay token
          this.presenters.quitLoading();
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

}
