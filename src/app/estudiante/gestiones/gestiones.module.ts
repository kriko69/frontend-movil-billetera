import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GestionesPageRoutingModule } from './gestiones-routing.module';

import { GestionesPage } from './gestiones.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GestionesPageRoutingModule
  ],
  declarations: [GestionesPage]
})
export class GestionesPageModule {}
