import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GestionesPage } from './gestiones.page';

describe('GestionesPage', () => {
  let component: GestionesPage;
  let fixture: ComponentFixture<GestionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GestionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
