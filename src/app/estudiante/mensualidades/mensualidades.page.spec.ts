import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MensualidadesPage } from './mensualidades.page';

describe('MensualidadesPage', () => {
  let component: MensualidadesPage;
  let fixture: ComponentFixture<MensualidadesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensualidadesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MensualidadesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
