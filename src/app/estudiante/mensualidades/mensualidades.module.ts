import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MensualidadesPageRoutingModule } from './mensualidades-routing.module';

import { MensualidadesPage } from './mensualidades.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MensualidadesPageRoutingModule
  ],
  declarations: [MensualidadesPage]
})
export class MensualidadesPageModule {}
