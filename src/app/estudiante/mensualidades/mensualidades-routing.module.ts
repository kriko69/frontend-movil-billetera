import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MensualidadesPage } from './mensualidades.page';

const routes: Routes = [
  {
    path: '',
    component: MensualidadesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MensualidadesPageRoutingModule {}
