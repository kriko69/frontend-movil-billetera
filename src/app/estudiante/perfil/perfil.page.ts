import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { estudiante } from 'src/models/estudiante';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  estudiante= new estudiante();
  habilitar=true;
  rol:string;
  ruta:string='';
  constructor(public parameters:ParametersServiceService,
    public eservice:EstudianteServiceService,
    public presenters:PresentersServiceService,
    public storage:Storage,
    public navController:NavController,
    public encrypt:CifradoNativoService) {
      
   }

  ngOnInit() {
    this.verificarRol();
    this.verificarTokenObteniendoPerfil();
  }

  Actualizar()
  {
    let fecha=this.estudiante.fecha_nacimiento.getFullYear()+'-'+(this.estudiante.fecha_nacimiento.getMonth()+1)+'-'+this.estudiante.fecha_nacimiento.getDate();
    let fecha_split=fecha.split('-');
    let mes=parseInt(fecha_split[1]);
    let mes_bien;
    if(mes<10)
    {
      mes_bien="0"+mes.toString();
    }else{
      mes_bien=mes.toString();
    }
    fecha=fecha_split[0]+'-'+mes_bien+'-'+fecha_split[2];
    this.estudiante.fecha_con_formato=fecha;
    console.log('estudiante enviado',this.estudiante);
    let info:any;
    this.presenters.presentLoading("Actualizando");
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.editarPerifl(tokenDes,this.estudiante).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log('info',info);
              if(info.estado=="exito")
              {
                this.presenters.presentAlert("Exito","Se actualizo el perfil correctamente.");
                this.verificarRolActualizar();
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                }else{
                  //hay un error
                  this.presenters.presentAlert('Error',info.mensaje);
                  this.navController.navigateRoot('/perfil');
                }
              }
              
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
              this.navController.navigateRoot('/perfil');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  editar()
  {
    console.log('toggle');
    this.habilitar=!this.habilitar;
  }

  cambiarPass()
  {
    this.parameters.setCarnetResetPassword(this.estudiante.carnet);
    this.parameters.setIrLogin(false);
    this.navController.navigateRoot('/cambiar-password');
  }

  verificarTokenObteniendoPerfil()
  {
    let info:any;
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.perfil(tokenDes).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              if(info.estado=="error")
              {
                //token expiro
                console.log('token expiro',info);
                this.storage.remove('token');
                this.storage.remove('rol');
                this.presenters.presentAlert("Error de inicio de sesion","La sesion expiro, inicie sesion nuevamente.");
                this.navController.navigateRoot('/login');
              }else{
                console.log('token bien',info);
                let fecha=info["data"].fecha_nacimiento.split('-');
                let dia=parseInt(fecha[2]);
                dia=dia+1;
                fecha[2]=dia.toString();
                //console.log(fecha);
                let fecha_nacimiento_usuario=fecha[0]+'-'+fecha[1]+'-'+fecha[2];
                this.estudiante.nombre=info["data"].nombre;
                this.estudiante.apellidos=info["data"].apellidos;
                this.estudiante.carnet=info["data"].carnet;
                this.estudiante.correo=info["data"].correo;
                this.estudiante.telefono=info["data"].telefono;
                this.estudiante.carrera=info["data"].carrera;
                this.estudiante.fecha_nacimiento=new Date(fecha_nacimiento_usuario);
                console.log('estudiante',this.estudiante);
              }
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.storage.remove('token');
              this.storage.remove('rol');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
              this.navController.navigateRoot('/login');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.quitLoading();
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  verificarRol()
  {
    this.storage.get('rol').then(
      (rol)=>{
        let rolDes=this.encrypt.desencriptar(rol);
        console.log('rol',rolDes);
        
        if(rolDes!=null)
        {
          if(rolDes=="estudiante"){
            this.ruta='menu-estudiante';
          }else{
            if(rolDes=="proveedor"){
              this.ruta='menu-proveedor';
            }else{
              this.presenters.presentAlert('Error del rol','Rol guardado incorrecto.');
              this.navController.navigateRoot('/login');
            }
          }
        }else{
          this.presenters.presentAlert('Error del rol','Inicie sesion nuevamente.');
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  verificarRolActualizar()
  {
    this.storage.get('rol').then(
      (rol)=>{
        let rolDes=this.encrypt.desencriptar(rol);
        console.log('rol',rol);
        
        if(rolDes!=null)
        {
          if(rolDes=="estudiante"){
            this.navController.navigateRoot('/menu-estudiante');
          }else{
            if(rolDes=="proveedor"){
              this.navController.navigateRoot('/menu-proveedor');
            }else{
              this.presenters.presentAlert('Error del rol','Rol guardado incorrecto.');
              this.navController.navigateRoot('/login');
            }
          }
        }else{
          this.presenters.presentAlert('Error del rol','Inicie sesion nuevamente.');
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  cambiarFoto()
  {
    this.navController.navigateRoot('/foto-perfil');
  }
}
