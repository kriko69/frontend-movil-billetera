import { Component, OnInit } from '@angular/core';
import { userLogin } from 'src/models/userLogin';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import {LoadingController, NavController, MenuController} from '@ionic/angular'
import { Storage } from '@ionic/storage';
import { ProveedorServiceService } from 'src/app/services/proveedor-service.service';
import { estudiante } from 'src/models/estudiante';
import * as forge from 'node-forge';
import * as keypair from 'keypair';
import{AndroidFingerprintAuth} from '@ionic-native/android-fingerprint-auth/ngx';
import { SecureStorage, SecureStorageObject } from '@ionic-native/secure-storage/ngx';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';
import { AES256 } from '@ionic-native/aes-256/ngx';
import { RsaService } from 'src/app/services/rsa.service';
import { CifradoHttpService } from 'src/app/services/cifrado-http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  login=new userLogin();
  estudiante=new estudiante();
  passtype="password";
  passicon="eye";
  load:any;
  pri;
  tiempo;
  image='/assets/';
  constructor(public presenters:PresentersServiceService,
    public eservice:EstudianteServiceService,
    public pservice:ProveedorServiceService,
    public loading:LoadingController,
    public navController:NavController,
    public parameters:ParametersServiceService,
    public storage:Storage,
    public menucontroller:MenuController,
    public androidFingerprintAuth: AndroidFingerprintAuth,
    public encrypt:CifradoNativoService,
    public AES:AES256,
    public rsaa:RsaService,
    public cipher:CifradoHttpService) { 

      this.menucontroller.enable(false,'first');
      this.storage.get('dark').then((dark)=>{
        if(dark==null) //si no existe
        {
          this.storage.set('dark',false); //lo creo
        }
      });
      
      
      
    }

  ngOnInit() {
    this.storage.set('intentos',0);
    this.verificarIntentos();
    this.storage.get('imageTheme').then(
      (image)=>{
       this.image=this.image+image;
      }
    );
    //verificar que tenga storage al inicio
    //this.rsa();
    //this.cifrado();
    //this.temporizador();
    let info:any;
    this.storage.get('token').then((token) => {
      this.storage.get('rol').then((rol) => {
        let tokenDes=this.encrypt.desencriptar(token);
        let rolDes=this.encrypt.desencriptar(rol);
        console.log('token inicio',tokenDes);
        console.log('rol inicio',rolDes);
        if(tokenDes!=null && rolDes!=null)
        {
          //hay token y rol
          /*if(rol=="estudiante")
          {
            //es estudiante
            this.navController.navigateRoot(['/menu-estudiante']);
          }else{
            //es proveedor
            this.navController.navigateRoot(['/menu-proveedor']);
          }*/this.inicioConHuella(rolDes,tokenDes);
        }
      });
    });
  }

  verificarIntentos()
  {
    this.storage.get('intentos').then(
      (intentos)=>{
        if (intentos!=null) {
          if (parseInt(intentos)==2) {
            this.presenters.presentLoadingLogin();
            let ahora = new Date();
            let segundos2=(ahora.getHours()*3600)+(ahora.getMinutes()*60)+ahora.getSeconds();
            this.storage.get('segundos-bloqueo').then(
              (segundos)=>{
                if(segundos!=null)
                {
                  if (parseInt(segundos)!=0) {
                    if(segundos<segundos2)
                    {
                      this.storage.set('intentos',0);
                      this.presenters.quitLoading();
                    }
                  }
                }
              }
            );
          }
        }
      }
    );
  }

  SignIn(){
    let info:any;
    let roles;
    this.presenters.presentLoading('Espere');
    console.log(this.login);
    this.eservice.login(this.login).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info);
        info=this.cipher.desencriptar(info);
        info=JSON.parse(info);
        console.log(info);
        this.presenters.quitLoading();
        if(info.estado=="Tokens creados con exito"){
          console.log('info', info);
          console.log('token',info["Token-Verificacion"]);
          this.storage.set('intentos',0);
          roles=info.roles.split(",");
          console.log(roles[0]);
          this.saveToken(info["Token-Verificacion"],roles[0]);
        }
        else{
          let intent=0;
          let aux;
          this.storage.get('intentos').then(
            (intentos)=>{
              if(intentos!=null)
              {
                if(parseInt(intentos)==2)
                {
                  let ahora = new Date();
                  let bloqueo=5*60;
                  let segundos=(ahora.getHours()*3600)+(ahora.getMinutes()*60)+ahora.getSeconds();
                  console.log('segundos actuales',segundos);
                  console.log('bloqueo',(segundos+bloqueo));
                  this.storage.set('segundos-bloqueo',(segundos+bloqueo));
                  let alert=this.presenters.presentAlertLogin("Error al ingresar","Por 3 intentos fallidos el inicio de sesion queda bloqueado por 5 minutos. Intente de nuevo pasado ese tiempo.");

                }else{
                  console.log('intentos',intentos);
                  aux=parseInt(intentos);
                  console.log(aux);
                  intent=aux+1;
                  this.storage.set('intentos',intent);
                  this.presenters.presentAlert("Error al ingresar",info.mensaje+" Le quedan "+(3-intent)+" intentos.");
                  console.log('error', info);
                } 
              }
            }
          );
          
          
        }
      }, (error: any)=> {
        this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert("error","no hay respuesta");
        }
    );
  }

  async getStorage(): Promise<any> {
    try {
        const result =  await this.storage.get('imageTheme');
        this.image=Object.assign(result);
        return result;
    }
    catch(e) { console.log(e) }
  }

  goMenu(rol){
    if(rol=="estudiante")
    {
      console.log('va a estudiante menu');
      this.storage.get('rol').then((val) => {
        console.log('rol guardado', val);
      });
      this.storage.get('token').then((token) => {
        console.log('token guardado', token);

      });
      this.navController.navigateRoot(['/menu-estudiante']);
      
      
    }else{
      console.log('va a proveedor menu');
      this.storage.get('rol').then((val) => {
        console.log('rol guardado', val);
      });
      this.storage.get('token').then((val) => {
        console.log('token guardado', val);
      });
      this.navController.navigateRoot(['/menu-proveedor']);
    }
    
  }

  seepass(){    
    if(this.passtype=='password')
    {
      this.passtype='text';
      this.passicon='eye-off';
    }
    else{
      this.passtype='password';
      this.passicon='eye';
    }
  }

  saveToken(token:string,rol:string){
    
    if(rol=='administrador' || rol=='superadmin')
    {
      this.presenters.presentAlert('Error','No puede ingresar con una cuenta de '+rol+'.');
    }else{
      this.parameters.setToken(token);
      this.parameters.setRol(rol);
      let tokenEnc=this.encrypt.encriptacion(token);
      let rolEnc=this.encrypt.encriptacion(rol);
      
      this.storage.set('token',tokenEnc);
      this.storage.set('rol',rolEnc);
      
      let info;
      this.storage.get('onesignal_guardado_DB').then(
        (guardado_en_DB)=>{
          this.storage.get('onesignal_id').then(
            (onesignal_id)=>{
              if(!guardado_en_DB)
              {
                if(onesignal_id!=null)
                {
                  //guardar onesignal id en la DB
                  this.eservice.guardarOneSignalId(token,onesignal_id).subscribe(
                    (data)=>{
                      info= Object.assign(data);
                      if(data.estado=='exito')
                      {
                        this.storage.set('onesignal_guardado_DB',true);
                      }
                    },
                    (error)=>{
                      console.log('Error al guardar onesignal id');
                    }
                  );
                  console.log('se guardo onesignal id en la DB');
                }
              }
            }
          );
        }
      );


      this.goMenu(rol);
    }
    
  }

  rsa(){
    /*let k = keypair();
    console.log('RSA',k);
    console.log('RSA public key',k.public);
    console.log('RSA private key',k.private);*/
    
    let rsa = forge.pki.rsa;
    rsa.generateKeyPair(2048, function(err, keypair) {
      const priv = keypair.privateKey;
      const pub = keypair.publicKey;
      // PEM serialize: public key
      const pubPem  = forge.pki.publicKeyToPem(pub);
      console.log("Public Key PEM:", pubPem);
      const pub2 = forge.pki.publicKeyFromPem(pubPem);

      // PEM serialize: private key
      const privPem  = forge.pki.privateKeyToPem(priv);
      console.log("PrivateKey PEM:", privPem);
      const priv2 = forge.pki.privateKeyFromPem(privPem);
      
      //encriptacion
      const encrypted = pub.encrypt("Hello World!");
      console.log('necesido: ',typeof encrypted);
      
      console.log("encrypted:", forge.util.encode64(encrypted));
      const decrypted = priv2.decrypt(encrypted,);
      console.log("decrypted:", decrypted);
      
      //firmar y validar
      const md = forge.md.sha256.create();
      md.update("Hello World!");
      const data = md.digest().bytes();
      const sign = priv.sign(md);
      console.log("sign:", forge.util.encode64(sign));

      const md2 = forge.md.sha256.create();
      md2.update("Hello world!");
      const data2 = md2.digest().bytes();
      console.log("verify:", pub.verify(data2, sign));

    });


    
  }

  inicioConHuella(rol:string,token:string){
    this.androidFingerprintAuth.isAvailable().then((result)=>
      {
        if(result.isAvailable){
          this.androidFingerprintAuth.encrypt({clientId:"clientid",username:"username",password:"password"})
          .then((result)=>{
            if(result.withFingerprint){
              if(rol=="estudiante")
              {
                //es estudiante
                this.navController.navigateRoot(['/menu-estudiante']);
              }else{
                //es proveedor
                this.navController.navigateRoot(['/menu-proveedor']);
              }
            }else if(result.withBackup){
              if(rol=="estudiante")
              {
                //es estudiante
                this.navController.navigateRoot(['/menu-estudiante']);
              }else{
                //es proveedor
                this.navController.navigateRoot(['/menu-proveedor']);
              }
            }
            
          },(err)=>{
            if(err === this.androidFingerprintAuth.ERRORS.FINGERPRINT_CANCELLED){
              this.storage.remove('token');
              this.storage.remove('rol');
              this.presenters.presentAlert('Cancelado','Ahora debe iniciar sesion nuevamente.')
            }
            else{
              this.storage.remove('token');
              this.storage.remove('rol');
              this.presenters.presentAlert('Error','Acceso con huella no disponible.'); 
            }
          });
        }else{
          // fingerprint auth isn't available
          this.storage.remove('token');
              this.storage.remove('rol');
          this.presenters.presentAlert('Error','Acceso con huella no disponible.');
        }
      }).catch(error => {
        this.storage.remove('token');
        this.storage.remove('rol');
        this.presenters.presentAlert('Error','Acceso con huella no disponible.');
      });
  }

  cifrado()
  {
    const keys= this.rsaa.createKeyPair();
    let key;
    keys.then(
      (data)=>{
        console.log(data);
        key=Object.assign(data);
      }
    );
    setTimeout(()=>{ 
      console.log(key.pubPem.length);  
      /*et enc=this.rsaa.encriptar(key.pubPem,'hola perro');
      console.log(enc);
      let text=this.rsaa.desencriptar(key.privPem,enc);
      console.log(text);*/
      
    },3000);
    
    let ci=this.encrypt.encriptacion('string');
    
    setTimeout(()=>{
      this.encrypt.desencriptar(ci);
    },3000);
  }


  temporizador()
  {
    var transactionTime = 0; //Initial time of timer
    var timeStamp = Math.floor(Date.now() / 1000);
    var deltaDelay = 1;

    var id=setInterval(function () {
        if (transactionTime != 0 && (Math.floor(Date.now() / 1000) - timeStamp) > deltaDelay) {
                transactionTime += (Math.floor(Date.now() / 1000) - timeStamp);
            }
            timeStamp = Math.floor(Date.now() / 1000);

            //Update your element with the new time.
            window.document.getElementById("timer").innerHTML = ''+transactionTime;
            /*if(transactionTime==10)
            {
              clearInterval(id);
            }*/
            console.log(transactionTime++);
            

    }, 1000);
  }
   
  

  
}
