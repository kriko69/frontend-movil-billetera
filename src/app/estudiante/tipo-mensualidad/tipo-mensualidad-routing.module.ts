import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoMensualidadPage } from './tipo-mensualidad.page';

const routes: Routes = [
  {
    path: '',
    component: TipoMensualidadPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TipoMensualidadPageRoutingModule {}
