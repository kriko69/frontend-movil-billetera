import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TipoMensualidadPage } from './tipo-mensualidad.page';

describe('TipoMensualidadPage', () => {
  let component: TipoMensualidadPage;
  let fixture: ComponentFixture<TipoMensualidadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoMensualidadPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TipoMensualidadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
