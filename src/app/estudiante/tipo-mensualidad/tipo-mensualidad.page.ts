import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
@Component({
  selector: 'app-tipo-mensualidad',
  templateUrl: './tipo-mensualidad.page.html',
  styleUrls: ['./tipo-mensualidad.page.scss'],
})
export class TipoMensualidadPage implements OnInit {

  constructor(public navController:NavController,
    public parameters:ParametersServiceService) { }

  ngOnInit() {
  }

  nueva()
  {
    this.parameters.setMmensualidadPagada(false);
    this.navController.navigateRoot('/gestiones');
  }

  pagadas()
  {
    this.parameters.setMmensualidadPagada(true);
    this.navController.navigateRoot('/gestiones');
  }

}
