import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TipoMensualidadPageRoutingModule } from './tipo-mensualidad-routing.module';

import { TipoMensualidadPage } from './tipo-mensualidad.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TipoMensualidadPageRoutingModule
  ],
  declarations: [TipoMensualidadPage]
})
export class TipoMensualidadPageModule {}
