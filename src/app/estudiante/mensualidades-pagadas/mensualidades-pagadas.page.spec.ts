import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MensualidadesPagadasPage } from './mensualidades-pagadas.page';

describe('MensualidadesPagadasPage', () => {
  let component: MensualidadesPagadasPage;
  let fixture: ComponentFixture<MensualidadesPagadasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensualidadesPagadasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MensualidadesPagadasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
