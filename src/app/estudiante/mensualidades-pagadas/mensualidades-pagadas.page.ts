import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import {Storage} from '@ionic/storage'
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-mensualidades-pagadas',
  templateUrl: './mensualidades-pagadas.page.html',
  styleUrls: ['./mensualidades-pagadas.page.scss'],
})
export class MensualidadesPagadasPage implements OnInit {

  gestion;
  mensualidades;
  cantidad;
  constructor(public menucontroller:MenuController,
    public eservice:EstudianteServiceService,
    public storage:Storage,
    public presenters:PresentersServiceService,
    public navController:NavController,
    public parameters:ParametersServiceService,
    public encrypt:CifradoNativoService) {
    
    this.menucontroller.enable(false,'first');
   }

   ngOnInit() {
    this.gestion=this.parameters.getGestion();
    console.log(this.gestion);
    
    this.obtenerMensualidadesNoPagadas();
  }


  obtenerMensualidadesNoPagadas()
  {
    let info;
    this.presenters.presentLoading('espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          //hay token
          this.eservice.obtenerMensualidadesPagadas(tokenDes,this.gestion.gestion_id).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log(info);
              if(info.mensaje=="el token ha expirado.")
              {
                console.log('token expiro',info);
                this.storage.remove('token');
                this.storage.remove('rol');
                this.presenters.presentAlert("Error de inicio de sesion","La sesion expiro, inicie sesion nuevamente.");
                this.navController.navigateRoot('/login');
              }else{
                if(info.estado=="exito" && info.data==null)
                {
                  info.data=[];
                  this.mensualidades=info.data;
                  this.cantidad=0;
                  console.log('mensualidades',info["data"].length);
                  this.presenters.presentAlert("Aviso importante",info.descripcion);
                  
                }else{
                  this.mensualidades=info.data;
                  this.cantidad=this.mensualidades.length;
                  console.log('mensualidades',this.mensualidades.length);
                }
              }
              
            },(error)=>{
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
            }
          );
        }else{
          //no hay token
          this.presenters.quitLoading();
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

}
