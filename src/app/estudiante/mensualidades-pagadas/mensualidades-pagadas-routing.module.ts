import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MensualidadesPagadasPage } from './mensualidades-pagadas.page';

const routes: Routes = [
  {
    path: '',
    component: MensualidadesPagadasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MensualidadesPagadasPageRoutingModule {}
