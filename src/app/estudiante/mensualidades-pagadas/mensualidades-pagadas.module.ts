import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MensualidadesPagadasPageRoutingModule } from './mensualidades-pagadas-routing.module';

import { MensualidadesPagadasPage } from './mensualidades-pagadas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MensualidadesPagadasPageRoutingModule
  ],
  declarations: [MensualidadesPagadasPage]
})
export class MensualidadesPagadasPageModule {}
