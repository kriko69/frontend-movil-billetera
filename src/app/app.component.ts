import { Component } from '@angular/core';

import { Platform, NavController, MenuController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DataService } from './services/data.service';
import { Observable } from 'rxjs';
import { ParametersServiceService } from './services/parameters/parameters-service.service';
import { estudiante } from 'src/models/estudiante';
import { Storage } from '@ionic/storage';
import { PresentersServiceService } from './services/presenters/presenters-service.service';
import { NetworkService } from './services/network.service';
import { PushService } from './services/push.service';
import { EstudianteServiceService } from './services/estudiante-service.service';
import { CifradoNativoService } from './services/cifrado-nativo.service';

declare var IRoot;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  menu:Observable<any>;
  image='../assets/';
  public onlineOffline: boolean = navigator.onLine;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private data:DataService,
    private navController:NavController,
    private menuCtrl:MenuController,
    private storage:Storage,
    private presenters:PresentersServiceService,
    public toast: ToastController,
    private push:PushService,
    private eservice:EstudianteServiceService,
    private encrypt:CifradoNativoService) {

      window.addEventListener ('offline', () => {
        // Hacer la tarea cuando no hay conexión a internet
        console.log('Estas offline');
        this.mostrarToastOffline();
        
      });
      window.addEventListener ('online', () => {
        // Hacer la tarea cuando regrese la conexión a internet
        console.log('Estas online');
        this.mostrarToastOnline()
      });
      this.initializeApp();
      this.menu=this.data.getMenuOps();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.checkDarkMode();
      this.imageTheme();
      this.intentos();
      this.push.configuracionInicial();
      if(this.platform.is('android')){
        this.verifyRoot();
      }
    });
  }

  intentos()
  {
    this.storage.get('intentos').then(
      (intentos)=>{
        if (intentos!=null) {
          
          
        } else {
          this.storage.set('intentos',0);
        }
      }
    );
    this.storage.get('segundos-bloqueo').then(
      (intentos)=>{
        if (intentos!=null) {
          
          
        } else {
          this.storage.set('segundos-bloqueo',0);
        }
      }
    );
  }
  imageTheme()
  { 
    
    this.storage.get('imageTheme').then(
      (image)=>{
        if (image!=null) {
          
          
        } else {
          this.storage.set('imageTheme','logo.png');
        }
      }
    );
  }

  checkDarkMode()
  {
    this.storage.set('firstDark',true); //firtstDark es mi bandera para que solo se ejecute una vez el cambio de color cuando habre la aplicacion y antes puso modo dark, esto reconoce si esta en dark y lo cambia al iniciar la aplicacion
    this.storage.get('firstDark').then((firstDark)=>{
      this.storage.get('dark').then(
        (dark)=>{
          if(dark!=null) //si existe
          {
            if(dark && firstDark) 
            {
              this.storage.set('firstDark',false); //con esto ya no deberia ejecutarse ese cambio de color la proxima vez
              document.body.classList.toggle('dark'); //cambio de color
            }
          }
        }
      );
    });
    
  }
  perfil()
  {
    console.log('hola desde perfil');
    this.menuCtrl.toggle();
    this.navController.navigateRoot('/perfil');
  }

  tema()
  {
    console.log('hola desde tema');
    this.menuCtrl.toggle();
    this.navController.navigateRoot('/tema');
  }

  
  salir()
  {
    console.log('hola desde salir');
    this.menuCtrl.toggle();
    //this.borrarOneSignalId();
    this.storage.remove('token');
    this.storage.remove('rol');
    this.storage.set('onesignal_guardado_DB',false);
    this.presenters.presentAlert('Exito','Se cerro sesion correctamente.')
    this.navController.navigateRoot('/login');
  }

  async mostrarToastOffline()
  {
    let tt =await this.toast.create({
      message: 'Sin conexion a internet :(',
      duration: 3000,
      position: 'top'
    });
    tt.present()
  }

  async mostrarToastOnline()
  {
    let tt =await this.toast.create({
      message: 'Con conexion a internet',
      duration: 3000,
      position: 'top'
    });
    tt.present()
  }

  borrarOneSignalId()
  {
    let info:any;
    this.presenters.presentLoading("Cerrando sesion");
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.resetOneSignal(tokenDes).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log('info',info);
              if(info.estado=="exito")
              {
                console.log('se borro onesignal id');
                
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                }else{
                  //hay un error
                  this.presenters.presentAlert('Error',info.mensaje);
                }
              }
              
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  verifyRoot()
  {
    if (typeof (IRoot) !== 'undefined' && IRoot) {
      IRoot.isRooted((data) => {
        if (data && data == 1) {
          this.presenters.presentAlert('Mensaje','El dispositivo esta rooteado y no es posible usar la aplicacion.');
          setTimeout(()=>{
            navigator['app'].exitApp();
          },8000); //despues de 8 segundos
        } else {
          //no pasa nada
        }
      }, (error) => {
        this.presenters.presentAlert('Mensaje','Error al verificar root');
      });
    }else{
      console.log('es undefined');
      
    }

  }

  open()
  {
    console.log('app.component hola');
  }
  
}
