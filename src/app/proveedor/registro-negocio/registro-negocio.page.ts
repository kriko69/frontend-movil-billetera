import { Component, OnInit } from '@angular/core';
import { negocio } from 'src/models/negocio';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-registro-negocio',
  templateUrl: './registro-negocio.page.html',
  styleUrls: ['./registro-negocio.page.scss'],
})
export class RegistroNegocioPage implements OnInit {

  negocio= new negocio();
  conteo:number=0;
  constructor(public parameters:ParametersServiceService,
    public eservice:EstudianteServiceService,
    public presenters:PresentersServiceService,
    public storage:Storage,
    public navController:NavController,
    public encrypt:CifradoNativoService) {

   }

  ngOnInit() {
  }

  registrarNegocio()
  {
    console.log(this.negocio);
    this.conteo=this.conteo+1;
    if(this.conteo>1)
    {

    }else{
      let aux= this.negocio.hora_inicio.split("T");
      let aux2=aux[1].split('.');
      let aux3=aux2[0].split(':');
      let hora_inicio=aux3[0]+":"+aux3[1];
      let aux4= this.negocio.hora_fin.split("T");
      let aux5=aux4[1].split('.');
      let aux6=aux5[0].split(':');
      let hora_fin=aux6[0]+":"+aux6[1];
      this.negocio.hora_inicio=hora_inicio;
      this.negocio.hora_fin=hora_fin;
    }
    
    console.log('negocio editado',this.negocio);
    this.register();
    
  }

  register()
  {
    let info:any;
    this.presenters.presentLoading("Registrando");
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.registroNegocio(tokenDes,this.negocio).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log('info',info);
              if(info.estado=="exito")
              {
                this.presenters.presentAlert("Exito","Se registro el negocio correctamente.");
                this.navController.navigateRoot('/menu-proveedor');
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                }else{
                  //hay un error
                  this.presenters.presentAlert('Error',info.mensaje);
                }
              }
              
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
              this.navController.navigateRoot('/registro-negocio');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

}
