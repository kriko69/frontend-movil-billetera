import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuProveedorPage } from './menu-proveedor.page';

const routes: Routes = [
  {
    path: '',
    component: MenuProveedorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuProveedorPageRoutingModule {}
