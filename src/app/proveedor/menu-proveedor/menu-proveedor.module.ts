import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuProveedorPageRoutingModule } from './menu-proveedor-routing.module';

import { MenuProveedorPage } from './menu-proveedor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuProveedorPageRoutingModule
  ],
  declarations: [MenuProveedorPage]
})
export class MenuProveedorPageModule {}
