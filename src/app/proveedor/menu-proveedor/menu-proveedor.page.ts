import { Component, OnInit } from '@angular/core';
import { proveedor } from 'src/models/proveedor';
import { Storage } from '@ionic/storage';
import { NavController, MenuController } from '@ionic/angular';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';
@Component({
  selector: 'app-menu-proveedor',
  templateUrl: './menu-proveedor.page.html',
  styleUrls: ['./menu-proveedor.page.scss'],
})
export class MenuProveedorPage implements OnInit {

  proveedor = new proveedor();
  saldo:any;
  linkFoto;
  constructor(public storage:Storage,
    public navController:NavController,
    public eservice:EstudianteServiceService,
    public parameters:ParametersServiceService,
    public menucontroller:MenuController,
    public presenters:PresentersServiceService,
    public encrypt:CifradoNativoService) {

      this.menucontroller.enable(true,'first');
   }

  ngOnInit() {
    //this.storage.remove('token');
    //this.storage.remove('rol');
    this.linkFoto=this.parameters.getimagenUsuarioProveedor();
    let info:any;
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.perfil(tokenDes).subscribe(
            (data)=>{
              info=Object.assign(data);
              if(info.estado=="error")
              {
                //token expiro
                console.log('token expiro',info);
                this.storage.remove('token');
                this.storage.remove('rol');
                this.presenters.presentAlert("Error de inicio de sesion","La sesion expiro, inicie sesion nuevamente.");
                this.navController.navigateRoot('/login');
              }else{
                console.log('token bien',info);
                this.linkFoto=this.linkFoto+info["data"].imagen;
                console.log('linkFoto',this.linkFoto);
                this.proveedor.usuario_id=info["data"].usuario_id;
                this.proveedor.nombre=info["data"].nombre;
                this.proveedor.apellidos=info["data"].apellidos;
                this.proveedor.carnet=info["data"].carnet;
                console.log('proveedor',this.proveedor);
                this.parameters.setNombreFotoPerfil(info["data"].imagen);
                this.parameters.setProveedorLogin(this.proveedor);
                this.parameters.setPerfilLogin(info);
                if(info["data"].password_default)
                {
                  console.log('password default');
                  this.presenters.alertaPasswordDefault(token);
                }
              }
            },(error)=>{
              //cuando no hay servidor
              console.log('error al hacer la peticion');
              this.storage.remove('token');
              this.storage.remove('rol');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
              this.navController.navigateRoot('/login');
              
            }
          );
          this.eservice.obtenerSaldo(tokenDes).subscribe(
            (data)=>{
              this.saldo=data.saldo;
              console.log('saldo',data.saldo);
              
            },(error)=>{
              //cuando no hay servidor
              console.log('error al hacer la peticion');
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );

    
  }
  

  doRefresh(event) {
    setTimeout(() => {
      this.storage.get('token').then(
        (token)=>{
          let tokenDes=this.encrypt.desencriptar(token);
          this.eservice.obtenerSaldo(tokenDes).subscribe(
            (data)=>{
              this.saldo=data.saldo;
              console.log('saldo',data.saldo);
              
            },(error)=>{
              //cuando no hay servidor
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
            }
          );
        }
      );
      event.target.complete();
    }, 2000);
  }


  irCuentas()
  {
    this.navController.navigateRoot('/cuentas');
  }
  irCodigoQr()
  {
    this.navController.navigateRoot('/negocios-qr');
  }
  irAfiliacion()
  {
    this.navController.navigateRoot('/registro-negocio');
  }
  irMisNegocios()
  {
    this.navController.navigateRoot('/mis-negocios');
  }

  refreshSaldo()
  {
    console.log('recargar saldo');
    this.presenters.presentLoading('Espere');
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        this.eservice.obtenerSaldo(tokenDes).subscribe(
          (data)=>{
            this.presenters.quitLoading();
            this.saldo=data.saldo;
            console.log('saldo',data.saldo);
            
          },(error)=>{
            //cuando no hay servidor
            this.presenters.quitLoading();
            console.log('error al hacer la peticion');
          }
        );
      },(error)=>{
        console.log('no hay token');
        this.presenters.quitLoading();
        this.presenters.presentAlert("Error de inicio de sesion", "Token no disponible, inicie sesion nuevamente.");
        this.navController.navigateRoot('/login');
      }
    );
  }

  irPerfil()
  {
    this.navController.navigateRoot('/perfil');
    
  }

}
