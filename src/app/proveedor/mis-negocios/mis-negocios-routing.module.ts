import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MisNegociosPage } from './mis-negocios.page';

const routes: Routes = [
  {
    path: '',
    component: MisNegociosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MisNegociosPageRoutingModule {}
