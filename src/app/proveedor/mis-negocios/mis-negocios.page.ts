import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { Storage } from '@ionic/storage';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-mis-negocios',
  templateUrl: './mis-negocios.page.html',
  styleUrls: ['./mis-negocios.page.scss'],
})
export class MisNegociosPage implements OnInit {

  negocios:any;
  cantidad:number;
  constructor(
    public navController:NavController,
    public parameters:ParametersServiceService,
    public eservice:EstudianteServiceService,
    public presenters:PresentersServiceService,
    public storage:Storage,
    public encrypt:CifradoNativoService) {

   }

  ngOnInit() {
    this.obtenerNegocios();
  }

  verPerfil(negocio)
  {
    console.log(typeof negocio);
    if(negocio.afiliacion==1){
      this.parameters.setTodoElNegocio(negocio);
      this.navController.navigateRoot('/perfil-negocios');
    }else{
      if(negocio.estado_afiliacion=='Rechazado')
      {
        this.parameters.setTodoElNegocio(negocio);
        this.navController.navigateRoot('/reintentar-afiliacion');
      }
    }
  }

  doRefresh(event) {
    setTimeout(() => {
      
      event.target.complete();
    }, 2000);
  }

  obtenerNegocios()
  {
    let info:any;
    this.presenters.presentLoading("Espere");
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.misNegocios(tokenDes).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log('info',info);
              if(info.estado=="exito")
              {
                if(info.data==null){
                  this.cantidad==0;
                  this.presenters.presentAlert('Exito',info.mensaje);
                }else{
                  console.log('todo bien',info.data);
                  this.negocios=info.data;
                  this.cantidad=this.negocios.length;
                }
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                }else{
                  //hay un error
                  this.presenters.presentAlert('Error',info.mensaje);
                  this.navController.navigateRoot('/menu-proveedor');
                }
              }
              
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
              this.navController.navigateRoot('/menu-proveedor');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

}
