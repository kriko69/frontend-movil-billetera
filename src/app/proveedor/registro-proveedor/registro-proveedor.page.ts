import { Component, OnInit } from '@angular/core';
import { proveedor } from 'src/models/proveedor';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { NavController } from '@ionic/angular';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { ProveedorServiceService } from 'src/app/services/proveedor-service.service';

@Component({
  selector: 'app-registro-proveedor',
  templateUrl: './registro-proveedor.page.html',
  styleUrls: ['./registro-proveedor.page.scss'],
})
export class RegistroProveedorPage implements OnInit {

  proveedor=new proveedor();
  passtype="password";
  passicon="eye";
  cerrarRules=true;
  constructor(public parameters:ParametersServiceService,
    public navController:NavController,
    public presenters:PresentersServiceService,
    public pservice:ProveedorServiceService
    ) { }

  ngOnInit() {
  }

  goNext()
  {
    let info:any;
    console.log(this.proveedor);
    console.log(this.validarPassword(this.proveedor.password));
    
    let es_valido=this.validarPassword(this.proveedor.password);
    if (es_valido) {
      console.log('es valido');
      
      this.parameters.setProveedorRegistro(this.proveedor);
      this.presenters.presentLoading('Espere');
      this.pservice.registroProveedor(this.proveedor).subscribe(
        data => {
          info= Object.assign(data);
          this.presenters.quitLoading();
          if(info.estado=="exito"){
            console.log('info', info);
            this.presenters.presentAlert("Cuenta creada con exito","ahora ya puede ingresar a la aplicacion.");
            this.navController.navigateRoot(['/login']);
          }
          else{
            console.log('error', info); 
            this.presenters.presentAlert("Error al crear la cuenta",info.mensaje);
          }
        }, (error: any)=> {
          this.presenters.quitLoading();
          console.log('error', error);
          this.presenters.presentAlert("error","no hay respuesta");
          }
      );
    } else {
      this.presenters.presentAlert('Error','El password no cumple con los requisitos requeridos.');
    }
    
    
  }

  seepass(){    
    if(this.passtype=='password')
    {
      this.passtype='text';
      this.passicon='eye-off';
    }
    else{
      this.passtype='password';
      this.passicon='eye';
    }
  }

  cerrar()
  {
    console.log('se cerro');
    this.cerrarRules=false;
  }

  validarPassword(password:string)
  {
    // se pone /expresion_regular/.test(palabra a examinar) y devuelve un booleano
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,}$/.test(password);
  }

 
}
