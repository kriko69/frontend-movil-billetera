import { Component, OnInit } from '@angular/core';
import { negocio } from 'src/models/negocio';
import { NavController, AlertController } from '@ionic/angular';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { Storage } from '@ionic/storage';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-perfil-negocios',
  templateUrl: './perfil-negocios.page.html',
  styleUrls: ['./perfil-negocios.page.scss'],
})
export class PerfilNegociosPage implements OnInit {

  negocio= new negocio();
  negocioData:any;
  habilitar=true;
  constructor(public navController:NavController,
    public parameters:ParametersServiceService,
    public eservice:EstudianteServiceService,
    public presenters:PresentersServiceService,
    public storage:Storage,
    public alerta:AlertController,
    public encrypt:CifradoNativoService) {

   }

  ngOnInit() {
    console.log(this.parameters.getTodoElNegocio());
    this.negocioData=this.parameters.getTodoElNegocio();
    this.negocio.nombre=this.negocioData.nombre;
    this.negocio.descripcion=this.negocioData.descripcion;
    this.negocio.hora_inicio=this.negocioData.hora_inicio;
    this.negocio.hora_fin=this.negocioData.hora_fin;
    this.negocio.negocio_id=this.negocioData.negocio_id;
  }

  editar()
  {
    console.log('toggle');
    this.habilitar=!this.habilitar;
  }
  Actualizar()
  {
    console.log(this.negocio);
    let info:any;
    this.presenters.presentLoading("Actualizando");
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.actualizarNegocio(tokenDes,this.negocio).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log('info',info);
              if(info.estado=="exito")
              {
                this.presenters.presentAlert('Exito','Se actualizo el negocio correctamente.');
                this.navController.navigateRoot('/menu-proveedor');
                
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                }else{
                  //hay un error
                  this.presenters.presentAlert('Error',info.mensaje);
                  this.navController.navigateRoot('/perfil-negocios');
                }
              }
              
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
              this.navController.navigateRoot('/menu-proveedor');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }
  
  async preguntar() {
    const alert = await this.alerta.create({
      header: '¿Está usted seguro?',
      message: 'Si edita el negocio debe volver a generar su codigo QR.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Cancelado',blah);
          }
        }, {
          text: 'Si',
          handler: () => {
            this.Actualizar();
          }
        }
      ]
    });

    await alert.present();
  }

}
