import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerfilNegociosPageRoutingModule } from './perfil-negocios-routing.module';

import { PerfilNegociosPage } from './perfil-negocios.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerfilNegociosPageRoutingModule
  ],
  declarations: [PerfilNegociosPage]
})
export class PerfilNegociosPageModule {}
