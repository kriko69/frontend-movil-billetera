import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerfilNegociosPage } from './perfil-negocios.page';

const routes: Routes = [
  {
    path: '',
    component: PerfilNegociosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerfilNegociosPageRoutingModule {}
