import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { Storage } from '@ionic/storage';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.page.html',
  styleUrls: ['./cuentas.page.scss'],
})
export class CuentasPage implements OnInit {

  cuentas:any;
  cantidad:number=0;
  constructor(public NavController:NavController,
    public parameters:ParametersServiceService,
    public presenters:PresentersServiceService,
    public eservice:EstudianteServiceService,
    public storage:Storage,
    public encrypt:CifradoNativoService) {

   }

  ngOnInit() {
    this.obtenerCuentas();
  }

  irTipoMovimiento(numero_cuenta:number)
  {
    this.parameters.setSoyProveedor(true);
    this.parameters.setNumeroCuentaMovimientosProveedor(numero_cuenta);
    this.NavController.navigateRoot('/tipo-movimientos');
  }

  obtenerCuentas()
  {
    let info:any;
    this.presenters.presentLoading("Espere");
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.obtenerMisCuentas(tokenDes).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log('info',info);
              if(info.estado=="exito")
              {
                if(info.data!=null)
                {
                  //hay data
                  console.log('data',info.data);
                  this.cuentas=info.data;
                  this.cantidad=this.cuentas.length;
                }else{
                  //la data es null
                  this.presenters.presentAlert('Usted no tiene negocios aun','No tiene una cuenta asociada a un negocio');
                  this.NavController.navigateRoot('/menu-proveedor');
                }
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.NavController.navigateRoot('/login');
                }else{
                  //hay un error
                  this.presenters.presentAlert('Error',info.mensaje);
                  this.NavController.navigateRoot('/menu-proveedor');
                }
              }
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
              this.NavController.navigateRoot('/tipo-movimientos');
              
            }
          );
          
        }else{
          //no hay token
          this.presenters.quitLoading();
          console.log('no hay token');
          this.storage.remove('token');
          this.storage.remove('rol');
          this.presenters.presentAlert("Error en el token","Token no disponible, inicie sesion nuevamente.");
          this.NavController.navigateRoot('/login');
        }
      }
    );
  }

}
