import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReintentarAfiliacionPageRoutingModule } from './reintentar-afiliacion-routing.module';

import { ReintentarAfiliacionPage } from './reintentar-afiliacion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReintentarAfiliacionPageRoutingModule
  ],
  declarations: [ReintentarAfiliacionPage]
})
export class ReintentarAfiliacionPageModule {}
