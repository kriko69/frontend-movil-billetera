import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReintentarAfiliacionPage } from './reintentar-afiliacion.page';

const routes: Routes = [
  {
    path: '',
    component: ReintentarAfiliacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReintentarAfiliacionPageRoutingModule {}
