import { Component, OnInit } from '@angular/core';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';                                                                  
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { Storage } from '@ionic/storage';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { NavController } from '@ionic/angular';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-reintentar-afiliacion',
  templateUrl: './reintentar-afiliacion.page.html',
  styleUrls: ['./reintentar-afiliacion.page.scss'],
})                                                              
export class ReintentarAfiliacionPage implements OnInit {

  negocio;
  constructor(public parameters:ParametersServiceService,
    public presenters:PresentersServiceService,
    public storage:Storage,
    public eservice:EstudianteServiceService,
    public navController:NavController,
    public encrypt:CifradoNativoService) { }

  ngOnInit() {
    this.negocio=this.parameters.getTodoElNegocio();
    console.log(this.negocio);

  }

  reintentar()
  {
    let info:any;
    this.presenters.presentLoading("Espere");
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          this.eservice.reintentarAfiliacion(tokenDes,this.negocio.negocio_id).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log('info',info);
              if(info.estado=="exito")
              {
                this.presenters.presentAlert("Exito","Se reenvio la solicitud de afiliacion.");
                this.navController.navigateRoot('/mis-negocios');
              }else{
                if(info.mensaje=="el token ha expirado.")
                {
                  //ha expirado el token
                  this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
                  this.storage.remove('token');
                  this.storage.remove('rol');
                  this.navController.navigateRoot('/login');
                }else{
                  //hay un error
                  this.presenters.presentAlert('Error',info.mensaje);
                  this.navController.navigateRoot('/mis-negocios');
                }
              }
              
            },(error)=>{
              //cuando no hay servidor
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor, intente mas tarde.");
              this.navController.navigateRoot('/mis-negocios');
              
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.presentAlert("Error de inicio de sesion","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

}
