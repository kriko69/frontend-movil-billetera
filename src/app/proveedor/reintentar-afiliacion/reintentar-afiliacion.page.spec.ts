import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReintentarAfiliacionPage } from './reintentar-afiliacion.page';

describe('ReintentarAfiliacionPage', () => {
  let component: ReintentarAfiliacionPage;
  let fixture: ComponentFixture<ReintentarAfiliacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReintentarAfiliacionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReintentarAfiliacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
