import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-negocios-qr',
  templateUrl: './negocios-qr.page.html',
  styleUrls: ['./negocios-qr.page.scss'],
})
export class NegociosQrPage implements OnInit {

  info:any;
  negocios:any;
  cantidad:any;
  constructor(public navController:NavController,
    public storage:Storage,
    public eservice:EstudianteServiceService,
    public parameters:ParametersServiceService,
    public presenters:PresentersServiceService,
    public encrypt:CifradoNativoService) {

   }

  ngOnInit() {
    this.obtenerNegocios();
  }

  irCodigoQR(negocio_id:number,numero_cuenta:number)
  {
    //console.log('negocio_id',negocio_id);
    //console.log('numero_cuenta',numero_cuenta);
    this.parameters.setNegocioIdParaQr(negocio_id);
    this.parameters.setNumeroCuentaIdParaQr(numero_cuenta);
    this.navController.navigateRoot('/codigo-qr');
  }

  obtenerNegocios()
  {
    this.presenters.presentLoading('Cargando negocios')
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          //hay token
          this.eservice.obtenerServicios(tokenDes).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              this.info=Object.assign(data);
              console.log(this.info);
              if(this.info.mensaje=="el token ha expirado.")
              {
                console.log('token expiro',this.info);
                this.storage.remove('token');
                this.storage.remove('rol');
                this.presenters.presentAlert("Error","La sesion expiro, inicie sesion nuevamente.");
                this.navController.navigateRoot('/login');
              }else{
                if(this.info.estado=="exito" && this.info.data==null)
                {
                  this.info.data=[];
                  this.negocios=this.info.data;
                  this.cantidad=0;
                  console.log('negocios',this.info["data"].length);
                  this.presenters.presentAlert("Aviso de Negocios",this.info.descripcion);
                  
                }else{
                  this.negocios=this.info.data;
                  this.cantidad=this.negocios.length;
                  console.log('negocios',this.negocios);
                }
              }
              
            },(error)=>{
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor.");
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.quitLoading();
          this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

}
