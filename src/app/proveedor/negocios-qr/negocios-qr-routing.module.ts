import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NegociosQrPage } from './negocios-qr.page';

const routes: Routes = [
  {
    path: '',
    component: NegociosQrPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NegociosQrPageRoutingModule {}
