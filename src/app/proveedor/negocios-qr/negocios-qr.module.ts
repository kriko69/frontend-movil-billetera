import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NegociosQrPageRoutingModule } from './negocios-qr-routing.module';

import { NegociosQrPage } from './negocios-qr.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NegociosQrPageRoutingModule
  ],
  declarations: [NegociosQrPage]
})
export class NegociosQrPageModule {}
