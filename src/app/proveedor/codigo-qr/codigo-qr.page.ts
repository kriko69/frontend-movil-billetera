import { Component, OnInit } from '@angular/core';
import { Base64ToGallery   } from '@ionic-native/base64-to-gallery/ngx';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-codigo-qr',
  templateUrl: './codigo-qr.page.html',
  styleUrls: ['./codigo-qr.page.scss'],
})
export class CodigoQrPage implements OnInit {

  qrData:string="null";
  elementType:'url'|'canvas'|'img'='img';
  negocio_id:number;
  numero_cuenta:number;
  hasWriteAccess: boolean = false;
  constructor(public base64:Base64ToGallery,
    public presenters:PresentersServiceService,
    public parameters:ParametersServiceService,
    public eservice:EstudianteServiceService,
    public storage:Storage,
    public navController:NavController,
    public android:AndroidPermissions,
    public encrypt:CifradoNativoService) {

     }

  ngOnInit() {
    this.negocio_id=this.parameters.getNegocioIdParaQr();
    this.numero_cuenta=this.parameters.getNumeroCuentaIdParaQr();
    console.log('negocio_id',this.negocio_id);
    console.log('numero_cuenta',this.numero_cuenta);
    this.obtenerDatosQR(this.negocio_id);
  }


  save()
  {

    console.log('se salvo');
    let img=document.getElementById('code');
    console.log(img);
    let imageData=img.getElementsByTagName('img')[0].src;
    console.log(imageData);
    
    /*let data= imageData.split(',')[1]; //la data del base64 image
    console.log(data);*/
    if(!this.hasWriteAccess)
    {
      this.checkPermission(imageData);
    }else{
      this.saveImage(imageData);
    }
    
    
  }

  obtenerDatosQR(negocio_id)
  {
    let info:any;
    let aux;
    this.presenters.presentLoading('Cargando negocios')
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          //hay token
          this.eservice.obtenerDatosParaQr(tokenDes,negocio_id).subscribe(
            (data)=>{
              this.presenters.quitLoading();
              info=Object.assign(data);
              console.log(info);
              if(info.mensaje=="el token ha expirado.")
              {
                console.log('token expiro',info);
                this.storage.remove('token');
                this.storage.remove('rol');
                this.presenters.presentAlert("Error","La sesion expiro, inicie sesion nuevamente.");
                this.navController.navigateRoot('/login');
              }else{
                if(info.estado=="exito")
                {
                  if(info.data!=null)
                  {
                    console.log('todo bien',info.data[0]);
                    //en un futuro encriptar cada segmento de la data
                    aux=info.data[0].negocio_id+"||"+info.data[0].dueno_id+"||"+info.data[0].carnet+"||"+this.numero_cuenta+"||"+info.data[0].nombre_negocio;
                    this.qrData=this.encrypt.encriptacion(aux);
                  }else{
                    //la data es null yel estado exito 
                    this.presenters.presentAlert('Algo paso','No se pudo obtener la informacion intentelo mas tarde. Es posible que usted o la cuenta figuren como eliminadas o el negocio no este vinculado a una cuenta. Para mas informacion comuniquese con un administrador de la universidad.');
                    this.navController.navigateRoot('/negocios-qr');
                  }
                  
                }else{
                  this.presenters.presentAlert('Error',info.mensaje);
                  this.navController.navigateRoot('/negocios-qr');
                }
              }
              
            },(error)=>{
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error","No se pudo contactar al servidor.");
            }
          );
        }else{
          //no hay token
          console.log('no hay token');
          this.presenters.quitLoading();
          this.presenters.presentAlert("Error","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  checkPermission(imageData)
  {
    this.android.checkPermission(this.android.PERMISSION.WRITE_EXTERNAL_STORAGE)
    .then((result) => {
      //tiene el permiso hasWriteAccess=true
      this.hasWriteAccess = result.hasPermission;
    },(err) => {
      //no tengo permiso entonces solicito
      this.android.requestPermission(this.android.PERMISSION.WRITE_EXTERNAL_STORAGE)
      .then(
        (status)=>{
          if(status.hasPermission)
          {
            this.saveImage(imageData);
          }
        }
      );
   });

   if (!this.hasWriteAccess) {
     //no tengo permiso entonces solicito
     this.android.requestPermission(this.android.PERMISSION.WRITE_EXTERNAL_STORAGE)
      .then(
        (status)=>{
          if(status.hasPermission)
          {
            this.saveImage(imageData);
          }
        }
      );
   }
  }

  saveImage(imageData)
  {
    this.base64.base64ToGallery(imageData,{prefix:'_img',mediaScanner:true})
    .then(
      (res)=>{
        //lo guardo correctamente
        console.log('lo guardo');
        this.presenters.presentAlert('Exito','Codigo QR guardado se guargo en tu galeria.');
      },(error)=>{
        console.log('error',error);
        this.presenters.presentAlert('Error',error);
      }
    );
  }

}
