import { Component, OnInit } from '@angular/core';
import { ParametersServiceService } from 'src/app/services/parameters/parameters-service.service';
import { Storage } from '@ionic/storage';
import { EstudianteServiceService } from 'src/app/services/estudiante-service.service';
import { PresentersServiceService } from 'src/app/services/presenters/presenters-service.service';
import { NavController } from '@ionic/angular';
import { CifradoNativoService } from 'src/app/services/cifrado-nativo.service';

@Component({
  selector: 'app-movimientos-proveedor',
  templateUrl: './movimientos-proveedor.page.html',
  styleUrls: ['./movimientos-proveedor.page.scss'],
})
export class MovimientosProveedorPage implements OnInit {

  tipoMovimiento:string;
  transaccions:any;
  cantidad:number;
  hora;
  fecha;
  constructor(public parameters:ParametersServiceService,
    public storage:Storage,
    public eservice:EstudianteServiceService,
    public presenters:PresentersServiceService,
    public navController:NavController,
    public encrypt:CifradoNativoService) { 
      this.obtenerFecha();
      
    }

  ngOnInit() {
    this.verificarRutaRol();

    this.tipoMovimiento=this.parameters.getTipoMovimiento();
    console.log(this.tipoMovimiento); //para mi o de mi
    if(this.tipoMovimiento=="de mi")
    {
      this.obtenerNumeroCuenta(this.tipoMovimiento);
    }else{
      this.obtenerNumeroCuenta(this.tipoMovimiento);
    }
  }

  verificarRutaRol()
  {
    this.storage.get('rol').then(
      (rol)=>{
        let rolDes=this.encrypt.desencriptar(rol);
        console.log('rol',rolDes);
        
        if(rolDes!=null)
        {
          if(rolDes=="estudiante"){
            this.parameters.setSoyProveedor(false);
          }else{
            if(rolDes=="proveedor"){
              this.parameters.setSoyProveedor(true);
            }else{
              this.presenters.presentAlert('Error del rol','Rol guardado incorrecto.');
              this.navController.navigateRoot('/login');
            }
          }
        }else{
          this.presenters.presentAlert('Error del rol','Inicie sesion nuevamente.');
          this.navController.navigateRoot('/login');
        }
      }
    );
  }


  obtenerNumeroCuenta(tipoMovimiento:string)
  {
    let info:any;
    let numero_cuenta=this.parameters.getNumeroCuentaMovimientosProveedor();
    this.presenters.presentLoading("Espere");
    this.storage.get('token').then(
      (token)=>{
        let tokenDes=this.encrypt.desencriptar(token);
        if (tokenDes!=null) {
          let info2:any;
          if (tipoMovimiento == "de mi") {
            this.eservice.obtenerMovimientosDeMi(tokenDes, numero_cuenta).subscribe(
              (data) => {
                this.presenters.quitLoading();
                info2 = Object.assign(data);
                console.log('info2', info2);
                if (info2.estado == "exito") {
                  if (info2.data == null) {
                    this.transaccions = [];
                    this.cantidad = 0;
                    this.presenters.presentAlert('Exito', 'Usted no realizo aun transacciones');
                  } else {
                    for (var i = 0; i < info2.data.length; i++) {
                      let fech = info2.data[i].fecha;
                      let sp = fech.split(' ')
                      let sp2 = sp[1].split(':');
                      let dis = parseInt(sp2[0]) - 4;
                      let correcto = sp[0] + ' ' + dis + ':' + sp2[1] + ':' + sp2[2];
                      //console.log(correcto);
                      info2.data[i].fecha = correcto;
                    }
                    this.transaccions = info2.data;
                    this.cantidad = this.transaccions.length;
                    console.log('transacciones length', this.cantidad);

                  }
                } else {
                  if (info.mensaje == "el token ha expirado.") {
                    //ha expirado el token
                    this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                    this.storage.remove('token');
                    this.storage.remove('rol');
                    this.navController.navigateRoot('/login');
                  } else {
                    //hay un error
                    this.presenters.presentAlert('Error', info.mensaje);
                    this.navController.navigateRoot('/tipo-movimientos');
                  }
                }
              }, (error) => {
                this.presenters.quitLoading();
                console.log('error al hacer la peticion');
                this.presenters.presentAlert("Error", "No se pudo contactar al servidor, intente mas tarde.");
                this.navController.navigateRoot('/tipo-movimientos');
              }
            );
          } else {

            this.eservice.obtenerMovimientosParaMi(tokenDes, numero_cuenta).subscribe(
              (data) => {
                this.presenters.quitLoading();
                info2 = Object.assign(data);
                console.log('info2', info2);
                if (info2.estado == "exito") {
                  if (info2.data == null) {
                    this.transaccions = [];
                    this.cantidad = 0;
                    this.presenters.presentAlert('Exito', 'Usted no realizo aun transacciones');
                  } else {
                    for (var i = 0; i < info2.data.length; i++) {
                      let fech = info2.data[i].fecha;
                      let sp = fech.split(' ')
                      let sp2 = sp[1].split(':');
                      let dis = parseInt(sp2[0]) - 4;
                      let correcto = sp[0] + ' ' + dis + ':' + sp2[1] + ':' + sp2[2];
                      //console.log(correcto);
                      info2.data[i].fecha = correcto;
                    }
                    this.transaccions = info2.data;
                    this.cantidad = this.transaccions.length;
                    console.log('transacciones length', this.cantidad);

                  }
                } else {
                  if (info.mensaje == "el token ha expirado.") {
                    //ha expirado el token
                    this.presenters.presentAlert("Error", "Token no disponible, inicie sesion nuevamente.");
                    this.storage.remove('token');
                    this.storage.remove('rol');
                    this.navController.navigateRoot('/login');
                  } else {
                    //hay un error
                    this.presenters.presentAlert('Error', info.mensaje);
                    this.navController.navigateRoot('/tipo-movimientos');
                  }
                }
              }, (error) => {
                this.presenters.quitLoading();
                console.log('error al hacer la peticion');
                this.presenters.presentAlert("Error", "No se pudo contactar al servidor, intente mas tarde.");
                this.navController.navigateRoot('/tipo-movimientos');
              }
            );

          }
          
        }else{
          //no hay token
          this.presenters.quitLoading();
          console.log('no hay token');
          this.storage.remove('token');
          this.storage.remove('rol');
          this.presenters.presentAlert("Error en el token","Token no disponible, inicie sesion nuevamente.");
          this.navController.navigateRoot('/login');
        }
      }
    );
  }

  obtenerFecha()
  {
    let hoy = new Date();
    let horas_string,minuto_string,segundo_string;
    let ano,mes,dia;
    let mes_string,dia_string;
    dia=hoy.getDay();
    mes=(hoy.getMonth()+1);
    ano=hoy.getFullYear();
    
    if(mes<10)
    {
      mes_string='0'+mes;
    }else{
      mes_string=mes;
    }

    if(dia<10)
    {
      dia_string='0'+dia;
    }else{
      dia_string=dia;
    }

    let hora=hoy.getHours();
    let minutos=hoy.getMinutes();
    let segundos=hoy.getSeconds();
    if(hora<10)
    {
      horas_string='0'+hora;
    }else{
      horas_string=hora;
    }

    if(minutos<10)
    {
      minuto_string='0'+minutos;
    }else{
      minuto_string=minutos;
    }

    if(segundos<10)
    {
      segundo_string='0'+segundos;
    }else{
      segundo_string=segundos;
    }
    
    this.hora=horas_string+":"+minuto_string+":"+segundo_string  ;
    this.fecha=dia_string+"/"+mes_string+"/"+ano;

  }

}
