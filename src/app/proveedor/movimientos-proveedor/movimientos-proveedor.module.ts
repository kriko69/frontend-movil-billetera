import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MovimientosProveedorPageRoutingModule } from './movimientos-proveedor-routing.module';

import { MovimientosProveedorPage } from './movimientos-proveedor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MovimientosProveedorPageRoutingModule
  ],
  declarations: [MovimientosProveedorPage]
})
export class MovimientosProveedorPageModule {}
