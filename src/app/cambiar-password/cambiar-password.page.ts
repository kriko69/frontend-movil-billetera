import { Component, OnInit } from '@angular/core';
import { ParametersServiceService } from '../services/parameters/parameters-service.service';
import { NgForm } from '@angular/forms';
import { PresentersServiceService } from '../services/presenters/presenters-service.service';
import { EstudianteServiceService } from '../services/estudiante-service.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-cambiar-password',
  templateUrl: './cambiar-password.page.html',
  styleUrls: ['./cambiar-password.page.scss'],
})
export class CambiarPasswordPage implements OnInit {

  passtype1="password";
  passicon1="eye";
  passtype2="password";
  passicon2="eye";
  data:Object={
    pass1:"",
    pass2:""
  };
  ruta;
  cerrarRules=true;
  constructor(public parameters:ParametersServiceService,
    public presenters:PresentersServiceService,
    public eservice:EstudianteServiceService,
    public navController:NavController) { 

    }

  ngOnInit() {
    let carnet=this.parameters.getCarnetResetPassword();
    console.log('carnet',carnet);
    let irLogin=this.parameters.getIrLogin();
    if(irLogin)
    {
      this.ruta='/login';
    }else{
      this.ruta='/perfil';
    }
  }

  cambiar()
  {
      if(this.data['pass1']==this.data['pass2'])
      {
        console.log('iguales');
        let es_valido=this.validarPassword(this.data['pass1']);
        if (es_valido) {
          let carnet=this.parameters.getCarnetResetPassword();
          console.log('carnet',carnet);
          let info:any;
          this.presenters.presentLoading("Espere");
          this.eservice.cambiarPassword(carnet,this.data['pass1'],this.data['pass2']).subscribe(
            (data)=>{
                info=Object.assign(data);
                console.log('info',info);
                this.presenters.quitLoading();
                if(info.estado=="exito")
                {
                  this.presenters.presentAlert("Exito","Se cambio el password correctamente.");
                  this.navController.navigateRoot('/login');
                }else{

                  this.presenters.presentAlert("Error",info.mensaje);
                }
            },(error)=>{
              this.presenters.quitLoading();
              console.log('error al hacer la peticion');
              this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
              this.navController.navigateRoot('/login');
            }
          );

        } else {
          this.presenters.presentAlert('Error','El password no cumple con los requisitos requeridos.');
        }
      }else{
          console.log('no iguales'); 
          this.presenters.presentAlert('Error','Las password no son iguales.');
      }
  }

  seepass1(){    
    if(this.passtype1=='password')
    {
      this.passtype1='text';
      this.passicon1='eye-off';
    }
    else{
      this.passtype1='password';
      this.passicon1='eye';
    }
  }
  seepass2(){    
    if(this.passtype2=='password')
    {
      this.passtype2='text';
      this.passicon2='eye-off';
    }
    else{
      this.passtype2='password';
      this.passicon2='eye';
    }
  }

  cerrar()
  {
    console.log('se cerro');
    this.cerrarRules=false;
  }
  validarPassword(password:string)
  {
    // se pone /expresion_regular/.test(palabra a examinar) y devuelve un booleano
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,}$/.test(password);
  }

}
