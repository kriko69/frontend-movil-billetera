import { NgModule } from '@angular/core';
import { SumarFechaPipe } from './sumar-fecha.pipe';

@NgModule({
declarations: [SumarFechaPipe],
imports: [],
exports: [SumarFechaPipe],
})

export class PipesModule {}