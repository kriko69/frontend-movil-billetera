import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sumarFecha'
})
export class SumarFechaPipe implements PipeTransform {

  transform(value: string): string {
    let aux=value.split(' ');
    let hora=aux[1].split(':');
    let numero=parseInt(hora[0]);
    numero=numero+4;
    hora[0]=numero.toString();
    return aux[0]+' '+hora[0]+':'+hora[1]+':'+hora[2];
  }

}
