import { Component, OnInit } from '@angular/core';
import { PresentersServiceService } from '../services/presenters/presenters-service.service';
import { EstudianteServiceService } from '../services/estudiante-service.service';
import { ParametersServiceService } from '../services/parameters/parameters-service.service';
import {NgForm} from '@angular/forms';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  data:Object={
    carnet:"",
    codigo_reset:""
  };
  bloquear=false;
  constructor(public presenters:PresentersServiceService,
    public eservice:EstudianteServiceService,
    public parameters:ParametersServiceService,
    public navController:NavController) { }

  ngOnInit() {
  }
  guardar()
  {
    console.log(this.data);
    this.parameters.setCarnetResetPassword(this.data['carnet']);
    let info:any;
    this.presenters.presentLoading("Verificando");
    this.eservice.verificarTokenResetPassword(this.data['carnet'],this.data['codigo_reset']).subscribe(
      (data)=>{
          info=Object.assign(data);
          console.log('info',info);
          this.presenters.quitLoading();
          if(info.estado=="exito")
          {
            this.parameters.setIrLogin(true);
            this.navController.navigateRoot('/cambiar-password');
          }else{
            this.presenters.presentAlert("Error",info.mensaje);
          }
      },(error)=>{
        this.presenters.quitLoading();
        console.log('error al hacer la peticion');
        this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
        this.navController.navigateRoot('/login');
      }
    );
    
  }

  enviarCodigo()
  {
    if(this.data['carnet']==null || this.data['carnet']=='')
    {
      this.presenters.presentAlert('Error','Debe ingresar un carnet valido.');
      
    }else{
      console.log(this.data);
      let info:any;
      this.presenters.presentLoading("Enviando");
      this.eservice.obtenerTokenResetPassword(this.data['carnet']).subscribe(
        (data)=>{
            info=Object.assign(data);
            console.log('info',info);
            this.presenters.quitLoading();
            if(info.estado=="exito")
            {
              this.bloquear=true;
              this.presenters.presentAlert("Correcto","Se envio un codigo de verificacion a tu correo.");
            }else{
              this.presenters.presentAlert("Error",info.mensaje);
            }
        },(error)=>{
          this.presenters.quitLoading();
          console.log('error al hacer la peticion');
          this.presenters.presentAlert("Error de inicio de sesion","No se pudo contactar al servidor.");
          this.navController.navigateRoot('/login');
        }
      );
    }
    
    
  }

}
